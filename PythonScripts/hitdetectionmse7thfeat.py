#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

def safe_ln(x, minval=0.001):
    return np.log(x.clip(min=minval))

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA

########## Uncomment this section if you want to load filenames from the script. Make changes to path.
# Uncomment only one of the mutfile options.
#mutfile = "/Users/newuser/Documents/newestdata/GMR_11F05_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_20C06_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/EXT_iav-GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_61D08_AD_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_38A10_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
mutfile = "/Users/newuser/Documents/newestdata/MZZ_ppk1d9GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
contrfile = "/Users/newuser/Documents/newestdata/pBDPGAL4U@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"


mut = open(mutfile,"rt")
contr = open(contrfile,"rt")
#######Uncomment this section if you want the user to enter the file names for mutants and wild type
#print "Enter the path and name of mutant file: "
#mutfile = raw_input()
#print = "Enter the path and name of wildtype file: "
#contrfile = raw_input()

#This section reads the files. We extracr in columns corresponding to hunches, 7,8 and 9.
# Also, substitute the "-10" for "NA". In that case, convert fo float in the if loop, before appending.
classifiers = dict(
	logistic=linear_model.LogisticRegression(C=1e5),
	gnb=GaussianNB(),
	lda=LDA(),
	qda=QDA()
	)
#ctmutturn = 0
#ctmutnoturn = 0
mutants = []
#mutantsno=[]
for line in mut:
	row = line.split()	
	row = list(row[i] for i in [0,1,2,3,4,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	del row[4]
	if rowi ==-10:
		rowend = 0 # = list(row[i] for i in [0,1,2,3,7,8])
		
	else:
		rowend = 1
	row=np.hstack([row,rowend])
	#print row
	#row.append(rowend)
	mutants.append(row)

#print mutants

#ctconturn = 0
#ctconnoturn = 0
controls = []
#controlsno=[]
for line in contr:	
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	del row[4]
	if rowi ==-10:
		rowend = 0
	else:
		rowend = 1
	row=np.hstack([row,rowend])
	controls.append(row)
	
	
print "These will be the classifiers used:"
mylegend= classifiers.keys()
print mylegend
			
lenmu = len(mutants)
lencon = len(controls)
print "there are a total of %d mutants and %d controls" %(lenmu,lencon)

controls = np.array(controls)
controls = controls[0:lenmu]
obs = mutants
obs.extend(controls)
obs = np.array(obs)
n_samples = len(obs)


tags = [0]*lenmu + [1]*lenmu
tags = np.array(tags)

# Normalizing the data
obs = (obs - np.mean(obs,axis=0))/np.std(obs,axis=0)

print "a total of %d observations, with %d controls and %d mutants" % (n_samples,lenmu,lenmu)

np.random.seed(0)
order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

numclass=len(classifiers.keys())
reptot = 100
msetrue = np.zeros(shape=(reptot,numclass))

ik = 0
for ik in range(reptot):
	mset = np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:.9 * n_samples]
	y_train = tags[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]
	y_test = tags[.9 * n_samples:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict_proba(X_test)
		nt = [y_predtot[i][y_test[i]] for i in range(len(y_predtot))]
		y_predtot = np.array(nt)
		mset[ii] = (1./(len(X_test)))*((1 - y_predtot)**2).sum()
		ii += 1		
	msetrue[ik] = mset
	ik += 1
	
msetruemed = np.median(msetrue,axis=0)

print" these are the median values of mse for each one of the classifiers:"
print msetruemed
print    

reptot = 10000#
msetot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	order = np.random.permutation(n_samples)
	obs = obs[order]
	X_train = obs[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]	
	order = np.random.permutation(n_samples)
	tags = tags[order]
	y_train = tags[:.9 * n_samples]
	y_test = tags[.9 * n_samples:]
	mset = np.zeros(numclass)
	ii = 0	
	for name, clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict_proba(X_test)
		nt=[y_predtot[i][y_test[i]] for i in range(len(y_predtot))]
		y_predtot = np.array(nt)
		mset[ii] = (1./(len(X_test)))*((1 - y_predtot)**2).sum()
		ii += 1		
	msetot[ik] = mset
	ik += 1


propmse = np.zeros(numclass)
for i in range(numclass):
	mseto = msetot[:,i]
	propmset = [j for j in mseto if j<= msetruemed[i]]
	lenmse = len(propmset)
	propmse[i] = lenmse*1./reptot
	
fig = pl.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(top=0.85)
ax.set_xlabel('Classifier')
ax.set_ylabel('Mse')

pl.boxplot(msetot,0,'')
pl.xticks([1,2,3,4],classifiers.keys())

for i in range(numclass):
	pl.plot(i+1, msetruemed[i],
        color='c', marker='*', markeredgecolor='k', markersize = 20)

for i in range(len(mylegend)):
	print "\mbox{%s} & %.3f & %.3f  \\ \hline"%(mylegend[i],msetruemed[i],propmse[i])
    
           
       
pl.show()
