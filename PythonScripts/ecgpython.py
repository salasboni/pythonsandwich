#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA

measfile = "/Users/newuser/Desktop/ecgmatlab/mcode/meas.txt"
speciesfile = "/Users/newuser/Desktop/ecgmatlab/mcode/indx.txt"
measopen = open(measfile,"rt")
speciesopen = open(speciesfile,"rt")

species=[]
meas = []
for line in measopen:
	row = line.split()	
	#print row
	row = [float(i) for i in row]	
	meas.append(row)
	
for line in speciesopen:
	row = line.split()
	row = [float(i) for i in row]	
	species.append(row)

obs = meas;
n_samples = len(obs)
print n_samples
n_samples=int(n_samples)
print n_samples

tags=species;
tags=np.array(tags)
tags=tags.T
#tags=tags.astype(np.int)
print tags
tags=int(tags)
np.random.seed(0)
#order = np.random.permutation(n_samples)
#type(obs)
#print order
#tags = tags[order].astype(np.float)

#obs = obs[order]
#print "these are the tags"
#print tags


tt=.9 * n_samples
tt=int(tt)#.9 * n_samples
X_train = obs[:tt]
y_train = tags[:tt]
X_test = obs[tt:]
y_test = tags[tt:]
clf = svm.SVC()

y_predtot = clf.fit(obs,tags).predict(obs)

acc=(100./(len(obs)))*(tags == y_predtot).sum()
y_predtot = clf.fit(X_train, y_train).predict(X_test)
acccv= (100./(len(X_test)))*(y_test == y_predtot).sum()

