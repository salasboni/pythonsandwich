#! /usr/bin/env ipython


# Code source: Gael Varoqueux
# Modified for Documentation merge by Jaques Grobler
# License: BSD
def makedatagau(n1,n2):
	mean1 = [0,0];mean2 = [0,0]
	cov1 = [[.3,0],[0,.3]];
	cov2 = [[.002,0],[0,5]]
	#cov2 = [[1,0],[0,1]]
	np.random.seed()
	z1 = np.random.multivariate_normal(mean1,cov1,n1)
	z2 = np.random.multivariate_normal(mean2,cov2,n2)
	return z1,z2
def makedatamoon():
	moons =datasets.make_moons(800,0,.3)
	z=moons[0];y=moons[1]
	z1=z[y==0];z2=z[y==1];
	order = np.random.permutation(len(z2))
	z2=z2[order]
	z2=z2[0:200]
	return z1,z2

def makedatacircle():
	circs=datasets.make_circles(900,1,.1)
	z=circs[0];y=circs[1]
	z1=z[y==0];z2=z[y==1];
	order = np.random.permutation(len(z2))
	z2=z2[order];z2=z2[0:200]
	order = np.random.permutation(len(z1))
	z1=z1[order]
	z1=z1[0:400]
	return z1,z2

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random
import matplotlib as mpl


# import some data to play with
#iris = datasets.load_iris()
#X = iris.data[:, :2]  # we only take the first two features.
#Y = iris.target
#Y = Y[50:]
#X = X[50:,:]
n1 = 200;n2 = 400
z1,z2 = makedatamoon()
#z1,z2 = makedatagau(n1,n2)
#z1,z2 = makedatacircle()
n1=len(z1);n2=len(z2)
X = np.vstack([z1,z2])
Y = [0]*n1 + [1]*n2
Y=np.array(Y)
h = .02  # step size in the mesh

classifiers = dict(
                   knn=neighbors.KNeighborsClassifier(),
                   logistic=linear_model.LogisticRegression(C=1e5),
                   svm=svm.LinearSVC(),#(C=1e5, loss='l1'),
                   svm_rbf = svm.SVC(kernel= 'rbf'),#(C=1e5, loss='l1'),
                   lda=LDA(),
                   qda=QDA()
                   )


fignum = 1
# we create an instance of Neighbours Classifier and fit the data.
col = ['xb','or']
for name, clf in classifiers.iteritems():
    clf.fit(X, Y)
    
    # Plot the decision boundary. For that, we will asign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    y_pred = clf.fit(X, Y).predict(X)
    print clf
    print "Number of correcly labeled points : %d" % (Y == y_pred).sum()
    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    pl.figure(fignum, figsize=(4, 3))
    pl.pcolormesh(xx, yy, Z, cmap=pl.cm.Paired)#gray)
    
    # Plot also the training points
    x1 = z1[:,0];y1=z1[:,1];
    pl.plot(x1,y1,'xb');#pl.axis('equal');
    x2 = z2[:,0];y2=z2[:,1];
    pl.plot(x2,y2,'or');#pl.axis('equal');
    #pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.jet)#pl.cm.gist_gray)
    # pl.xlabel('Sepal length')
    #pl.ylabel('Sepal width')
    pl.title(name)
    pl.xlim(xx.min(), xx.max())
    pl.ylim(yy.min(), yy.max())
    pl.xticks(())
    pl.yticks(())
    fignum += 1
    na = '2moons'+name+'.png';
    pl.savefig(na)
#pl.show()
