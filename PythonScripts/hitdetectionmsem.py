#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

def safe_ln(x, minval=0.001):
    return np.log(x.clip(min=minval))

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA

########## Uncomment this section if you want to load filenames from the script. Make changes to path.
# Uncomment only one of the mutfile options.
#mutfile = "/Users/newuser/Documents/newestdata/GMR_11F05_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_20C06_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/EXT_iav-GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_61D08_AD_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_38A10_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
mutfile = "/Users/newuser/Documents/newestdata/MZZ_ppk1d9GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
contrfile = "/Users/newuser/Documents/newestdata/pBDPGAL4U@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"


#######Uncomment this section if you want the user to enter the file names for mutants and wild type
#print "Enter the path and name of mutant file: "
#mutfile = raw_input()
#print = "Enter the path and name of wildtype file: "
#contrfile = raw_input()

#This section reads the files. We extracr in columns corresponding to hunches, 7,8 and 9.
# Also, substitute the "-10" for "NA". In that case, convert fo float in the if loop, before appending.

mut = open(mutfile,"rt")
contr = open(contrfile,"rt")

ctmutturn = 0
ctmutnoturn = 0
mutants = []
for line in mut:
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctmutnoturn += 1
	else:
		ctmutturn += 1
		mutants.append(row)

ctconturn = 0
ctconnoturn = 0
controls = []	
for line in contr:	
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctconnoturn += 1
	else:
		ctconturn += 1
		controls.append(row)
		
##### for normal datasets, where you are not taking away columns or removing observations based on missing features, 
# use commented code below to load data instead
#mutants = []
#for line in mut:
#	row = line.split()
#	row = [float(i) for i in row]		
#	mutants.append(row)
#
#controls = []	
#for line in contr:	
#	row = line.split()
#	row = [float(i) for i in row]	
#	controls.append(row)				

lenmu = len(mutants)
lencon = len(controls)
print "there are a total of %d mutants and %d controls" %(lenmu,lencon)

propmut = float(ctmutturn)/(ctmutturn + ctmutnoturn)
propcont = float(ctconturn)/(ctconturn + ctconnoturn)
newcon = int((ctmutturn + ctmutnoturn)*propcont)

print "the proportion of mutants that turn and controls that turn are %f and %f"%(propmut,propcont)
print "the new number of controls will be %d" % newcon

controls = controls[0:newcon]
lencon = newcon

obs = mutants
obs.extend(controls)
obs=np.array(obs)
n_samples = len(obs)

tags = [0]*lenmu + [1]*lencon
tags=np.array(tags)

##### Uncomment for getting rid of outliers
#Getting rid of outliers and normalizing the data
#m=2
#obs = [abs(obs - np.mean(obs,axis=0)) < m * np.std(obs,axis=0)]

# Normalizing the data
obs = (obs - np.mean(obs,axis=0))/np.std(obs,axis=0)

print "a total of %d observations, with %d controls and %d mutants" % (n_samples,newcon,lenmu)

np.random.seed(0)
order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order]#.astype(np.float)

classifiers = dict(
	logistic=linear_model.LogisticRegression(C=1e5),
	gnb=GaussianNB(),
	lda=LDA(),
	qda=QDA()
	)
print "These will be the classifiers used:"
print classifiers.keys()

numclass=len(classifiers.keys())
reptot = 100
msetrue = np.zeros(shape=(reptot,numclass))
mtrue = np.zeros(shape=(reptot,numclass))

ik = 0
for ik in range(reptot):
	mset = np.zeros(numclass)
	mt = np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:.9 * n_samples]
	y_train = tags[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]
	y_test = tags[.9 * n_samples:]
	#print y_test
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict_proba(X_test)
		#print np.shape(y_predtot)
		nt=[y_predtot[i][y_test[i]] for i in range(len(y_predtot))]
		y_predtot = np.array(nt)#y_predtot[y_test]		
		#print type(y_predtot)
		#print np.shape(1-y_predtot)
		mset[ii] = (1./(len(X_test)))*((1 - y_predtot)**2).sum()
		mt[ii] = -(1./(len(X_test)))*(safe_ln(y_predtot)).sum()
		ii += 1
		
	msetrue[ik] = mset
	mtrue[ik] = mt
	ik += 1
	
msetruemed = np.median(msetrue,axis=0)
mtruemed = np.median(mtrue,axis=0)

print" these are the median values of mse and m for each one of the classifiers:"
print msetruemed
print mtruemed    

print "how many iterations for computing the p-value?: "   
reptotN = int(raw_input())
reptot = reptotN

#acctot =  np.zeros(shape=(reptot,numclass))
msetot = np.zeros(shape=(reptot,numclass))
mtot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	order = np.random.permutation(n_samples)
	obs = obs[order]
	X_train = obs[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]	
	order = np.random.permutation(n_samples)
	tags = tags[order]
	y_train = tags[:.9 * n_samples]
	y_test = tags[.9 * n_samples:]
	mset = np.zeros(numclass)
	mt = np.zeros(numclass)
	ii = 0	
	for name, clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict_proba(X_test)
		nt=[y_predtot[i][y_test[i]] for i in range(len(y_predtot))]
		y_predtot = np.array(nt)
		#y_predtot = y_predtot[y_test]
		mset[ii] = (1./(len(X_test)))*((1 - y_predtot)**2).sum()
		mt[ii] = -(1./(len(X_test)))*(safe_ln(y_predtot)).sum()
		ii += 1
		
	msetot[ik] = mset
	mtot[ik] = mt
	ik += 1

#### Uncomment to save file of accuracies computed in p--value estimation
#mutfile = mutfile.split('@',1)
#mutfile = mutfile[0]
#mutfile = mutfile.split('/')
#mutfile = mutfile[-1]
#mutfile = mutfile + '_acccv.txt'
#np.savetxt(mutfile,acccvtot)

propmse = np.zeros(numclass)
propm = np.zeros(numclass)

for i in range(numclass):
	mseto = msetot[:,i]
	mto = mtot[:,i]
	propmset = [j for j in mseto if j<= msetruemed[i]]
	propmt = [j for j in mto if j<= mtruemed[i]]	
	lenmse = len(propmset)
	lenm = len(propmt)
	propmse[i] = lenmse*1./reptot
	propm[i] = lenm*1./reptot
	print classifiers.keys()[i]+": the p-value of the mse's being smaller than %f is %f" % (msetruemed[i],propmse[i])
	print classifiers.keys()[i]+": the p-value of the m's being smaller than %f is %f" % (mtruemed[i],propm[i])

fig = pl.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(top=0.85)
ax.set_xlabel('Classifier')
ax.set_ylabel('Mse')

pl.boxplot(msetot,0)#,notch=1)#,usermedians=medians)#medians)
pl.xticks([1,2,3,4],classifiers.keys())

for i in range(numclass):
	pl.plot(i+1, msetruemed[i],
        color='c', marker='*', markeredgecolor='k', markersize = 20)

fig = pl.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(top=0.85)
ax.set_xlabel('Classifier')
ax.set_ylabel('M')


pl.boxplot(mtot,0,'')#,usermedians=medians)
#medians)
pl.xticks([1,2,3,4],classifiers.keys())

for i in range(numclass):
	pl.plot(i+1, mtruemed[i],
        color='c', marker='*', markeredgecolor='k', markersize = 20)


pl.show()
