#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com
		
def makedatagau(n1,n2):
	mean1 = [0,0];mean2 = [0,0]
	cov1 = [[1,0],[0,1]];
	#cov2 = [[1,0],[0,1]]
	cov2 = [[1,0],[0,1]]
	np.random.seed()
	z1 = np.random.multivariate_normal(mean1,cov1,n1)
	z2 = np.random.multivariate_normal(mean2,cov2,n2)
	return z1,z2

def makedatamoon():
	moons =datasets.make_moons(800,0,.3)
	z=moons[0];y=moons[1]
	z1=z[y==0];z2=z[y==1];
	order = np.random.permutation(len(z2))
	z2=z2[order]
	z2=z2[0:200]
	return z1,z2

def makedatacircle():
	circs=datasets.make_circles(900,1,.1)
	z=circs[0];y=circs[1]
	z1=z[y==0];z2=z[y==1];
	order = np.random.permutation(len(z2))
	z2=z2[order];z2=z2[0:200]
	order = np.random.permutation(len(z1))
	z1=z1[order]
	z1=z1[0:400]
	return z1,z2



import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp, binom, mannwhitneyu, chisquare, ksprob, ksone, distributions

markers = ['o','x','d','*','^','s']
colors = ['r', 'g', 'b', 'c','m','y','k'];n1 = 200;n2 = 400
#z1,z2 = makedatagau(n1,n2)
#z1,z2 = makedatamoon()
z1,z2 = makedatacircle()

n1=len(z1);n2=len(z2)
print "There are a total of %d obs in pop 1 and %d obs in pop 2" %(n1,n2)


fig = pl.figure()
x1 = z1[:,0];y1=z1[:,1];
pl.plot(x1,y1,'xb');#pl.axis('equal');
x2 = z2[:,0];y2=z2[:,1];
pl.plot(x2,y2,'or');#pl.axis('equal');

obs = np.vstack([z1,z2])
tags = [0]*n1 + [1]*n2
tags=np.array(tags)

n_samples = len(obs)#n1+n2

#obs = (obs - np.mean(obs,axis=1))/np.std(obs,axis=1)

order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

#print tags

classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000
pmin = min(n1*1./n_samples,n2*1./n_samples)
print pmin
prtest=.1;
prtrain = 1 - prtest;
ntr = n_samples*prtrain
nte = n_samples*prtest
########

rtot = np.zeros(shape=(reptot,numclass))
ptot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	pt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:ntr];y_train = tags[:ntr]
	X_test = obs[ntr:];y_test = tags[ntr:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		rt[ii] = (1./nte)*((y_test != y_predtot).sum())
		ii += 1
	
	rtot[ik] = rt
	ptot[ik] = binom.cdf(nte*rt, nte,pmin)#pt
	ik += 1

x = np.random.binomial(nte,pmin,reptot)
aa = (1./(nte))*np.sort(x)   
aa1 = np.arange( len(aa)*1.0)/len(aa)

fig = pl.figure()
ax = fig.add_subplot(111)
ax.plot(aa,aa1,'k',linewidth=3.0,label='binomial dist')
makeverys=[24,19,22]

ik=0
for ik in range(numclass):
	x1 = rtot[:,ik]
	sorted = np.sort(x1)
	pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[ik],marker=markers[ik],markevery=makeverys[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	

fig = pl.figure()
ax = fig.add_subplot(111)

#x = np.random.binomial(nte,pmin,reptot) #stats.norm.cdf(dx1, p1, std1))
sorted = np.sort(x)                        
aa = binom.cdf(sorted, nte,pmin)
bb = np.arange( len(aa)*1.0)/len(aa)
ax.step(aa,bb,'k',linewidth=3.0,label='binomial dist')
ik=0

for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	ysorted =np.arange( len(sorted)*1.0)/len(sorted)
	lastx = sorted[-1]
	x1 = sorted[1:]-sorted[:-1];x2 = ysorted
	x2=x2[:-1]; xa=x1*x2;	
	pl.plot( sorted, ysorted ,colors[ik],marker=markers[ik],markevery=makeverys[ik],label=mylegend[ik] )
	pl.legend(loc=4)

pl.show()
