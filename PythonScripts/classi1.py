#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA

#controls = load(datasetcontrols)
#mutants = load(datasetmutants)

#controlstags = zeros # ???
#mutantstags = ones #???

#obs = merge(controls, mutants) # CHECK THIS COMMAND
#tags = merge(controlstags, mutantstags)

#n_samples = len(obs);

#np.random.seed(0)
#order = np.random.permutation(n_sample)
#obs = obs[order]
#tags = tags[order]

#obs_train = obs_[:.9*n_samples]
#tags_train = tags_[:.9*n_samples]
#obs_test = obs_[.9*n_samples:]
#tags_test= tags_[.9*n_samples:]



digits = datasets.load_digits()
X_digits = digits.data
y_digits = digits.target

n_samples = len(X_digits)

X_train = X_digits[:.9 * n_samples]
y_train = y_digits[:.9 * n_samples]
X_test = X_digits[.9 * n_samples:]
y_test = y_digits[.9 * n_samples:]


# add lda and qda, maybe naive bayes!!!!
classifiers = dict(
    knn=neighbors.KNeighborsClassifier(),
    logistic=linear_model.LogisticRegression(C=1e5),
    svm=svm.LinearSVC(C=1e5, loss='l1'),
    gnb=GaussianNB(),
    lda=LDA()
    )

fignum = 1

for name, clf in classifiers.iteritems():
    y_pred=clf.fit(X_train, y_train).predict(X_test)
    
    #y_pred = clf.fit(X, Y).predict(X)
    print "Number of correcly labeled points : %d" % (y_test == y_pred).sum()


    fignum += 1

pl.show()




digits = datasets.load_digits()
X_digits = digits.data
y_digits = digits.target

n_samples = len(X_digits)

X_train = X_digits[:.9 * n_samples]
y_train = y_digits[:.9 * n_samples]
X_test = X_digits[.9 * n_samples:]
y_test = y_digits[.9 * n_samples:]

knn = neighbors.KNeighborsClassifier()
logistic = linear_model.LogisticRegression()

print('KNN score: %f' %
        knn.fit(X_train, y_train).score(X_test, y_test))
print('LogisticRegression score: %f' %
        logistic.fit(X_train, y_train).score(X_test, y_test))


# import some data compare
iris = datasets.load_iris()
X = iris.data[:, :2]  # we only take the first two features.
Y = iris.target
print Y.shape
h = .02  # step size in the mesh

classifiers = dict(
    knn=neighbors.KNeighborsClassifier(),
    logistic=linear_model.LogisticRegression(C=1e5),
    svm=svm.LinearSVC(C=1e5, loss='l1'),
    )


fignum = 1
# we create an instance of Neighbours Classifier and fit the data.
for name, clf in classifiers.iteritems():
    clf.fit(X, Y)

    # Plot the decision boundary. For that, we will asign a color to each
    # point in the mesh [x_min, m_max]x[y_min, y_max].
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
            np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    pl.figure(fignum, figsize=(4, 3))
    pl.pcolormesh(xx, yy, Z, cmap=pl.cm.Paired)

    # Plot also the training points
    pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Paired)
    pl.xlabel('Sepal length')
    pl.ylabel('Sepal width')

    pl.xlim(xx.min(), xx.max())
    pl.ylim(yy.min(), yy.max())
    pl.xticks(())
    pl.yticks(())
    fignum += 1

pl.show()
