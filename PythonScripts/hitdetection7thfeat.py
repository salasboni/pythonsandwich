#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA

########## Uncomment this section if you want to load filenames from the script. Make changes to path.
# Uncomment only one of the mutfile options.
#mutfile = "/Users/newuser/Documents/newestdata/GMR_11F05_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_20C06_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/EXT_iav-GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_61D08_AD_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_38A10_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
mutfile = "/Users/newuser/Documents/newestdata/MZZ_ppk1d9GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
contrfile = "/Users/newuser/Documents/newestdata/pBDPGAL4U@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"


#######Uncomment this section if you want the user to enter the file names for mutants and wild type
#print "Enter the path and name of mutant file: "
#mutfile = raw_input()
#print = "Enter the path and name of wildtype file: "
#contrfile = raw_input()

#This section reads the files. We extracr in columns corresponding to hunches, 7,8 and 9.
# Also, substitute the "-10" for "NA". In that case, convert fo float in the if loop, before appending.
classifiers = dict(
	knn=neighbors.KNeighborsClassifier(),
	logistic=linear_model.LogisticRegression(C=1e5),
	svm=svm.LinearSVC(),#(C=1e5, loss='l1'),
	gnb=GaussianNB(),
	lda=LDA(),
	qda=QDA()
	)
print "These will be the classifiers used:"
print classifiers.keys()

mut = open(mutfile,"rt")
contr = open(contrfile,"rt")

#ctmutturn = 0
#ctmutnoturn = 0
mutants = []
#mutantsno=[]
for line in mut:
	row = line.split()	
	row = list(row[i] for i in [0,1,2,3,4,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	del row[4]
	if rowi ==-10:
		rowend = 0 # = list(row[i] for i in [0,1,2,3,7,8])
		
	else:
		rowend = 1
	row=np.hstack([row,rowend])
	#print row
	#row.append(rowend)
	mutants.append(row)

#print mutants

#ctconturn = 0
#ctconnoturn = 0
controls = []
#controlsno=[]
for line in contr:	
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	del row[4]
	if rowi ==-10:
		rowend = 0
	else:
		rowend = 1
	row=np.hstack([row,rowend])
	controls.append(row)
		
##### for normal datasets, where you are not taking away columns or removing observations based on missing features, 
# use commented code below to load data instead
#mutants = []
#for line in mut:
#	row = line.split()
#	row = [float(i) for i in row]		
#	mutants.append(row)
#
#controls = []	
#for line in contr:	
#	row = line.split()
#	row = [float(i) for i in row]	
#	controls.append(row)				

lenmu = len(mutants)
lencon = len(controls)
print "there are a total of %d mutants and %d controls" %(lenmu,lencon)

#print "the new number of controls will be %d" % newcon
controls = np.array(controls)
controls = controls[0:lenmu]
#lencon = newcon

obs = mutants
obs.extend(controls)
obs = np.array(obs)
n_samples = len(obs)

tags = [1]*lenmu + [2]*lenmu
tags=np.array(tags)

##### Uncomment for getting rid of outliers
#Getting rid of outliers and normalizing the data
#m=2
#obs = [abs(obs - np.mean(obs,axis=0)) < m * np.std(obs,axis=0)]

# Normalizing the data
obs = (obs - np.mean(obs,axis=0))/np.std(obs,axis=0)

print "a total of %d observations, with %d controls and %d mutants" % (n_samples,lenmu,lenmu)

np.random.seed(0)
order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)


numclass=len(classifiers.keys())
reptot = 100
acccvtrue = np.zeros(shape=(reptot,numclass))
ik = 0
for ik in range(reptot):
	acccv=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:.9 * n_samples]
	y_train = tags[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]
	y_test = tags[.9 * n_samples:]
	ii = 0
	for name,clf in classifiers.iteritems():	
	#y_predtot = clf.fit(obs,tags).predict(obs)
	#acc[ii] = (100./(len(obs)))*(tags == y_predtot).sum()
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		acccv[ii] = (100./(len(X_test)))*(y_test == y_predtot).sum()
		ii += 1
		
	acccvtrue[ik] = acccv
	ik += 1
	
acctruemed = np.median(acccvtrue,axis=0)
#acctruemin = np.(acccvtrue,axis = 0)
print" these are the median values of accuracy for each one of the classifiers:"
print acctruemed

#print "how many iterations for computing the p-value?: "
#reptotN = int(raw_input())
reptot = 10000#reptotN

#acctot =  np.zeros(shape=(reptot,numclass))
acccvtot = np.zeros(shape=(reptot,numclass))
ik = 0

for ik in range(reptot):
	order = np.random.permutation(n_samples)
	obs = obs[order]
	X_train = obs[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]	
	order = np.random.permutation(n_samples)
	tags = tags[order]
	y_train = tags[:.9 * n_samples]
	y_test = tags[.9 * n_samples:]
	#acc = np.zeros(numclass)
	acccv = np.zeros(numclass)

	ii = 0	
	for name, clf in classifiers.iteritems():	
		#y_predtot = clf.fit(obs,tags).predict(obs)
		#acc[ii] = (100./(len(obs)))*(tags == y_predtot).sum()
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		acccv[ii] = (100./(len(X_test)))*(y_test == y_predtot).sum()
		ii += 1
	
	#acctot[ik] = acc
	acccvtot[ik] = acccv
	ik += 1

medians = np.median(acccvtot,axis=0)

#### Uncomment to save file of accuracies computed in p--value estimation
#mutfile = mutfile.split('@',1)
#mutfile = mutfile[0]
#mutfile = mutfile.split('/')
#mutfile = mutfile[-1]
#mutfile = mutfile + '_acccv.txt'
#np.savetxt(mutfile,acccvtot)

propacc = np.zeros(numclass)
for i in range(numclass):
	accto = acccvtot[:,i]
	propa = [j for j in accto if j>= acctruemed[i]]	
	lenp = len(propa)
	propacc[i] = float(lenp)/reptot
	print classifiers.keys()[i]+": the p-value of the accuracies being greater than %f is %f" % (acctruemed[i],propacc[i])

mylegend = classifiers.keys()

fig = pl.figure()
ax = fig.add_subplot(111)
fig.subplots_adjust(top=0.85)
ax.set_xlabel('Classifier')
ax.set_ylabel('Accuracy')

pl.boxplot(acccvtot,notch=1,usermedians=medians)
pl.xticks([1,2,3,4,5,6],classifiers.keys())

for i in range(numclass):
	pl.plot(i+1, acctruemed[i],
        color='c', marker='*', markeredgecolor='k', markersize = 20)
     
for i in range(len(mylegend)):
	print "\mbox{ %s} & %.3f & %.3f \\ \hline" % (mylegend[i],acctruemed[i],propacc[i])
     
       
pl.show()

