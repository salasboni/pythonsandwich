#!/usr/bin/python

from __future__ import print_function

import numpy as np
import pylab as pl
from sklearn.utils import shuffle
from sklearn.metrics import roc_curve, auc
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import matplotlib as mpl
from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn import svm

print(__doc__)


def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx #array[idx]

random_state = np.random.RandomState(0)

# Import some data to play with
# 
# fileMeas = "/Users/salasboni/pythonsandwich/Data/meas.txt"
# fileIndx = "/Users/salasboni/pythonsandwich/Data/indx.txt"
# 
# X = np.loadtxt(fileMeas)
# y = np.loadtxt(fileIndx)
# 
# n_samples, n_features = X.shape
# 
# 
# # shuffle and split training and test sets
# X, y = shuffle(X, y, random_state=random_state)
# half = int(n_samples / 2)
# prop = .8
# 
# X_train, X_test = X[:prop*n_samples], X[prop*n_samples:]
# y_train, y_test = y[:prop*n_samples], y[prop*n_samples:]


# fileMeasTr = "/Users/salasboni/pythonsandwich/Data/meas_train.txt"
# fileIndxTr = "/Users/salasboni/pythonsandwich/Data/indx_train.txt"
# fileMeasTe = "/Users/salasboni/pythonsandwich/Data/meas_test.txt"
# fileIndxTe = "/Users/salasboni/pythonsandwich/Data/indx_test.txt"

fileMeasTr = "/Users/salasboni/ecgProject/Data/meas_train1.txt"
fileIndxTr = "/Users/salasboni/ecgProject/Data/indx_train1.txt"
fileMeasTe = "/Users/salasboni/ecgProject/Data/meas_test1.txt"
fileIndxTe = "/Users/salasboni/ecgProject/Data/indx_test1.txt"

X_train = np.loadtxt(fileMeasTr)
y_train = np.loadtxt(fileIndxTr)
X_test = np.loadtxt(fileMeasTe)
y_test = np.loadtxt(fileIndxTe)



# Set the parameters by cross-validation
tuned_parameters = [{'kernel': ['rbf'], 'gamma': [10,1,1e-2,1e-3, 1e-4],
                     'C': [1000,100,10,1,1e-2,1e-3, 1e-4],'class_weight':[{1:10},{1:5}],'probability':[True]},                  
                    {'kernel': ['linear'], 'C': [1000,100,10,1,1e-2,1e-3, 1e-4
                    ],'class_weight':[{1:10},{1:5}],'probability':[True]}]

scores = ['recall']

for score in scores:
    print("# Tuning hyper-parameters for %s" % score)
    print()
    clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5, scoring=score)
    clf.fit(X_train, y_train)

    print("Best parameters set found on development set:\n")
    print(clf.best_estimator_)
    print("\n Grid scores on development set:\n")
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() / 2, params))
    print()

    print("Detailed classification report:\n")
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()
    probas_ = clf.predict_proba(X_test)
    fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
    rocauc = auc(fpr, tpr)
    # Plot ROC curve
    fig = pl.figure()
    pl.clf()
    pl.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % rocauc)
    pl.plot([0, 1], [0, 1], 'k--')
    pl.xlim([0.0, 1.0])
    pl.ylim([0.0, 1.0])
    pl.xlabel('False Positive Rate')
    pl.ylabel('True Positive Rate')
    pl.title('Receiver operating characteristic example')
    pl.legend(loc="lower right")

