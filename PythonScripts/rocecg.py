#!/usr/bin/env ipython

import numpy as np
import pylab as pl
from sklearn import svm, datasets
from sklearn.utils import shuffle
from sklearn.metrics import roc_curve, auc

random_state = np.random.RandomState(0)


# Import some data to play with

fileMeas = "/Users/newuser/Desktop/ecgmatlab/mcode/meas.txt"
fileIndx = "/Users/newuser/Desktop/ecgmatlab/mcode/indx.txt"

X = np.loadtxt(fileMeas)
y = np.loadtxt(fileIndx)

n_samples, n_features = X.shape

# shuffle and split training and test sets
X, y = shuffle(X, y, random_state=random_state)
half = int(n_samples / 2)
prop = .9
X_train, X_test = X,X#X[:prop*n_samples], X[prop*n_samples:]
y_train, y_test = y,y#y[:prop*n_samples], y[prop*n_samples:]

# Run classifier
classifier = svm.SVC(C=1.0, kernel='rbf', probability=True)
probas_ = classifier.fit(X_train, y_train).predict_proba(X_test)

# Compute ROC curve and area the curve
fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
roc_auc = auc(fpr, tpr)
print("Area under the ROC curve : %f" % roc_auc)

# Plot ROC curve
pl.clf()
pl.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
pl.plot([0, 1], [0, 1], 'k--')
pl.xlim([0.0, 1.0])
pl.ylim([0.0, 1.0])
pl.xlabel('False Positive Rate')
pl.ylabel('True Positive Rate')
pl.title('Receiver operating characteristic example')
pl.legend(loc="lower right")
pl.show()
