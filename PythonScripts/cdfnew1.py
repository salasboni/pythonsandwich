#! /usr/bin/env ipython

import numpy as np
#import scikits.statsmodels as sm
import matplotlib.pyplot as plt

sample = np.random.uniform(0, 1, 200)
sorted=np.sort( sample )
plt.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted) )
plt.show()
#ecdf = sm.tools.ECDF(sample)

#x = np.linspace(min(sample), max(sample))
#y = ecdf(x)
#plt.step(x, y)

