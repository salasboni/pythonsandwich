#!/usr/bin/python

import numpy as np
import pylab as pl
from sklearn import svm, datasets
from sklearn.utils import shuffle
from sklearn.metrics import roc_curve, auc
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import matplotlib as mpl
from sklearn import preprocessing
from sklearn.svm import SVC



def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx #array[idx]

random_state = np.random.RandomState(0)

# 
# 
fileMeas = "/Users/salasboni/EcgProject/Data/meas1.txt"
fileIndx = "/Users/salasboni/EcgProject/Data/indx1.txt"

X = np.loadtxt(fileMeas)
y = np.loadtxt(fileIndx)

n_samples, n_features = X.shape
print("the number of samples is %f " % n_samples)
print("the number of features is %f " % n_features)
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

# shuffle and split training and test sets
X, y = shuffle(X, y, random_state=random_state)
half = int(n_samples / 2)
prop = .8

X_train, X_test = X[:prop*n_samples], X[prop*n_samples:]
y_train, y_test = y[:prop*n_samples], y[prop*n_samples:]

# Import some data to play with
# # 
# fileMeasTr = "/Users/salasboni/EcgProject/Data/meas_train.txt"
# fileIndxTr = "/Users/salasboni/EcgProject/Data/indx_train.txt"
# fileMeasTe = "/Users/salasboni/EcgProject/Data/meas_test.txt"
# fileIndxTe = "/Users/salasboni/EcgProject/Data/indx_test.txt"
# 
# X_train = np.loadtxt(fileMeasTr)
# y_train = np.loadtxt(fileIndxTr)
# X_test = np.loadtxt(fileMeasTe)
# y_test = np.loadtxt(fileIndxTe)
# 
# n1,n2 = X_train.shape
# print("The size of the training set is %f rows and %f columns" % (n1,n2))
# n1,n2 = X_test.shape
# print("The size of the test set is %f rows and %f columns" % (n1,n2))


classifiers = dict(
	logistic100 = linear_model.LogisticRegression(C=1e2,class_weight={1:20}),
	svm = svm.SVC(C=10,kernel= 'linear', probability=True,class_weight={1:20}),
	svm_rbf = svm.SVC(C=10, cache_size=200,coef0=0.0, degree=3,gamma=0.0001, kernel='rbf', probability=True,random_state=None, shrinking=True, tol=0.001, verbose=False,class_weight={1: 20})
# 	qda = QDA(priors = np.array([.01,.99]))
	)

mylegend = classifiers.keys()

print("These will be the classifiers used:")
print(mylegend)

n_classifiers = len(classifiers.keys())

ii = 0
sensitivity = np.array([1.,.98,.95,.9,.8])

for name,clf in classifiers.iteritems():	

	# Run classifier
	probas_ = clf.fit(X_train, y_train).predict_proba(X_test)

	# Compute ROC curve and area the curve
	fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
	roc_auc = auc(fpr, tpr)
	print("Area under the ROC curve using %s is : %f" % (mylegend[ii],roc_auc))

	# Compute specificity for different sensitivity values
	for j in range(len(sensitivity)):
		idx = find_nearest(tpr,sensitivity[j])
		print("At a sensitivity of %0.2f we get a specificity of %0.2f" % (sensitivity[j],1-fpr[idx]))
	
	print("~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	
	# Plot ROC curve
	fig = pl.figure()
	pl.clf()
	pl.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
	pl.plot([0, 1], [0, 1], 'k--')
	pl.xlim([0.0, 1.0])
	pl.ylim([0.0, 1.0])
	pl.xlabel('False Positive Rate')
	pl.ylabel('True Positive Rate')
	pl.title('Receiver operating characteristic example with %s' % mylegend[ii])
	pl.legend(loc="lower right")

	ii += 1


pl.show()
