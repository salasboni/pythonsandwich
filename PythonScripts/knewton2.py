#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

import numpy as np
import pylab as pl
import csv 

studentfile = "/Users/newuser/astudentData.csv"
cr = csv.reader(open(studentfile,"rb"))
cr.next() 

questnum = []
for row in cr:    
	questnum.append(row)

dquest = {}
for question, student, answer in questnum:
	dquest.setdefault(question, {})[student] = answer
	
numques = len(dquest.keys())
print "the number of questions is %d" % numques	

numques = np.zeros(shape=(len(dquest.keys()))) # number of times each question is asked
numquesans = np.zeros(shape=(len(dquest.keys()))) # proportion of times each question is answered correctly

i = 0
questno = [] # questions to remove
for question in dquest.iteritems():
	newdic = dquest[question[0]]
	newdicv=newdic.values()		
	# number of times each question is asked
	numques[i]=len(newdicv) 
	if len(newdicv)<10:
		questno.append(question[0])	
	# proportion of times each question is answered correctly
	numquesans[i] =((np.array(newdicv).astype(np.int)).sum())*1./len(newdicv)
	#remove questions most of the students answer correctly or incorrectly
	if numquesans[i]<.1 or numquesans[i]>.8:
		questno.append(question[0])
	i += 1


# make histograms of D_0 vs D_1 for questions 27 and 64 

dquest = {};dstud = {}
for question, student, answer in questnum:
	dquest.setdefault(question, {})[student] = answer
	dstud.setdefault(student,{})[question] = answer
	
newdic = dquest['656']	
numstud = len(newdic.keys())
numquesin = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, including
numquesex = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, excluding
numques0 = [];numques1 = [];
j = 0
for student in newdic.keys():	
	newdicv = dstud[student]
	newdicv = newdicv.values()
	numquesin[j] =((np.array(newdicv).astype(np.int)).sum())*1./(len(newdicv))
	numquesex[j] =((np.array(newdicv).astype(np.int)).sum() - np.array(newdic[student]).astype(np.int))*1./(len(newdicv)-1)
	if newdic[student]=='0':
		numques0.append(numquesex[j])
	else:
		numques1.append(numquesex[j])
	j += 1
	
fig = pl.figure()	
n, bins, patches = pl.hist(numques0, 20, facecolor='blue', alpha=0.75)	
n, bins, patches = pl.hist(numques1, 20, facecolor='red', alpha=0.75)	

newdic = dquest['12151']	
numstud = len(newdic.keys())
numquesin = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, including
numquesex = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, excluding
numques0 = [];numques1 = [];
j = 0
for student in newdic.keys():	
	newdicv = dstud[student]
	newdicv = newdicv.values()
	numquesin[j] =((np.array(newdicv).astype(np.int)).sum())*1./(len(newdicv))
	numquesex[j] =((np.array(newdicv).astype(np.int)).sum() - np.array(newdic[student]).astype(np.int))*1./(len(newdicv)-1)
	if newdic[student]=='0':
		numques0.append(numquesex[j])
	else:
		numques1.append(numquesex[j])
	j += 1
	
fig = pl.figure()	
n, bins, patches = pl.hist(numques0, 20, facecolor='blue', alpha=0.75)	
n, bins, patches = pl.hist(numques1, 20, facecolor='red', alpha=0.75)

newdic = dquest['2504']	
numstud = len(newdic.keys())
numquesin = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, including
numquesex = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, excluding
numques0 = [];numques1 = [];
j = 0
for student in newdic.keys():	
	newdicv = dstud[student]
	newdicv = newdicv.values()
	numquesin[j] =((np.array(newdicv).astype(np.int)).sum())*1./(len(newdicv))
	numquesex[j] =((np.array(newdicv).astype(np.int)).sum() - np.array(newdic[student]).astype(np.int))*1./(len(newdicv)-1)
	if newdic[student]=='0':
		numques0.append(numquesex[j])
	else:
		numques1.append(numquesex[j])
	j += 1
	
fig = pl.figure()	
n, bins, patches = pl.hist(numques0, 20, facecolor='blue', alpha=0.75)	
n, bins, patches = pl.hist(numques1, 20, facecolor='red', alpha=0.75)	

newdic = dquest['13074']	
numstud = len(newdic.keys())
numquesin = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, including
numquesex = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, excluding
numques0 = [];numques1 = [];
j = 0
for student in newdic.keys():	
	newdicv = dstud[student]
	newdicv = newdicv.values()
	numquesin[j] =((np.array(newdicv).astype(np.int)).sum())*1./(len(newdicv))
	numquesex[j] =((np.array(newdicv).astype(np.int)).sum() - np.array(newdic[student]).astype(np.int))*1./(len(newdicv)-1)
	if newdic[student]=='0':
		numques0.append(numquesex[j])
	else:
		numques1.append(numquesex[j])
	j += 1
	
fig = pl.figure()	
n, bins, patches = pl.hist(numques0, 20, facecolor='blue', alpha=0.75)	
n, bins, patches = pl.hist(numques1, 20, facecolor='red', alpha=0.75)
### en : make histograms 



i = 0
dstud = {}
for question, student, answer in questnum:
	if question not in questno:
		dstud.setdefault(student,{})[question] = answer

studsno = []

for ik in range(3):
	
	# remove bad students (less than 5 questions asked) and bad questions
	#print questno

	questnumnew=[]
	for row in questnum:
		ques1=row[0]
		stud1 = [1]
		if ques1 not in questno:# and stud1 not in studsno:
			questnumnew.append(row)
	
	# make new dictionaries		
	dquest = {};dstud = {}
	for question, student, answer in questnumnew:
		dquest.setdefault(question, {})[student] = answer
		dstud.setdefault(student,{})[question] = answer
		

	print "the number of students is %d" % len(dstud.keys())	
	print "the number of questions is %d" % len(dquest.keys())	
	
	# test the question pool
	numrep = 1
	scorestot = []#np.zeros(shape=(numrep,numclass))
	for i in range(numrep): # pick number of repetitions		
		scorestu = []
		for student in dstud.iteritems():
			newdic = dstud[student[0]]
			newdicv = newdic.values()
			if len(newdicv)>4:
				order = np.random.permutation(len(newdicv))
				newdicv = np.array(newdicv).astype(np.int)
				newdicv = newdicv[order];newdicv = newdicv[0:5];
				scorestu.append(newdicv.sum()*1./len(newdicv))
		print type(scorestu)		
		scorestot = np.hstack([scorestot,scorestu])			
	# histogram of students grades using 5 questions
	fig = pl.figure()
	n, bins, patches = pl.hist(scorestot, 12, facecolor='blue')# alpha=0.75)	

	scorestu = np.zeros(shape=(len(dstud.keys()))) # score each student has
	
	i = 0
	for student in dstud.iteritems():
		newdic = dstud[student[0]]
		newdicv=newdic.values()		
		scorestu[i] =((np.array(newdicv).astype(np.int)).sum())*1./len(newdicv)
		i += 1
	
	meanstu = np.mean(scorestu)
	stdstu = np.std(scorestu)
		
	distances = np.zeros(shape=(len(dquest.keys()))) # distribution of scores of students who were asked this question, including
	distances2 = np.zeros(shape=(len(dquest.keys()))) # distribution of scores of students who were asked this question, including
	
	i = 0	
	for question in dquest.iteritems():
		newdic = dquest[question[0]]	
		numstud = len(newdic.keys())
		numquesin = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, including
		numquesex = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, excluding
		numques0 = [];numques1 = [];
		j = 0
		for student in newdic.keys():	
			newdicv = dstud[student]
			newdicv = newdicv.values()
			numquesin[j] =((np.array(newdicv).astype(np.int)).sum())*1./(len(newdicv))
			numquesex[j] =((np.array(newdicv).astype(np.int)).sum() - np.array(newdic[student]).astype(np.int))*1./(len(newdicv)-1)
			if newdic[student]=='0':
				numques0.append(numquesex[j])
			else:
				numques1.append(numquesex[j])
			j += 1
		
		meanin = np.mean(numquesin);meanex = np.mean(numquesex)
		stdin = np.std(numquesin);stdex = np.std(numquesin)
		distin = np.abs((meanin - meanstu)*1./(stdin + stdstu))
		distex = np.abs((meanex - meanstu)*1./(stdex + stdstu))
		distances2[i] = distex - distin
		mean0 = np.mean(numques0);mean1 = np.mean(numques1)
		std0 = np.std(numques0);std1 = np.std(numques1)
		dist = (mean1 - mean0)*1./(std0 + std1)
		distances[i]=dist
		newdic['distance'] = dist
		dquest[question[0]] = newdic
		i += 1
		
	distances = distances[np.logical_not(np.isnan(distances))]
	distances2 = distances2[np.logical_not(np.isnan(distances2))]
	
	# sig_2 measurement
	distancesmin = np.sort(distances2)
	distin = sorted(range(len(distances2)), key=lambda k: distances2[k])
	distin = np.array(distin).astype(np.int);distin = distin[0:49]
	ques = dquest.keys();ques = [ques[ij] for ij in distin]
	#questno.extend(ques)
	
	# sig_1 measurement
	distances = distances[distances>0] 	# removing the negative ones
	distancesmin = np.sort(distances)
	distin = sorted(range(len(distances)), key=lambda k: distances[k])
	distin = np.array(distin).astype(np.int);distin = distin[0:49]
	ques = dquest.keys();ques = [ques[ij] for ij in distin]
	questno.extend(ques)

	ik += ik

pl.show()



