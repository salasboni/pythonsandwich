#para leer un archivo con datos estilo sm
#

from numpy import*

#
#  leetabla(), L. Salas , 20110630
#  leepar()
#

def leetabla (archivo, *columnas):  # lee un archivo datos ordenado por columnas, 
				  #    varias columnas a la vez, numero variable

    indcol=range(len(columnas))
    var = []
    for icol in indcol : var= var+[[]]


    for line in file(archivo):
        line = line.split()	# separa la linea en sus componentes
        if (len(line)>0) :	# si la linea no esta vacia

            if line[0][0:1] != '#':  # cuando linea no empiece con comentario
					
	     # ver si es un numero o un string
                for icol in indcol :
                    col = columnas[icol]-1  # col empieza en 0, no en 1
                    try:			# trata de ver si pasa por numero
                        var0=float(line[col])  #no funciona con eval, decidir float
                    except ValueError:
                        var0=line[col]   # no pasa, es string.
                    var[icol].append(var0) # agregar este elemento a la lista	
                if len(indcol) > 1 :
                    res = []
                    for icol in indcol : res= res+[array(var[icol])] # haz una lista de listas de arreglos
                else:
                    res = array(var[0])
   
    return res

################################################################################

def leepar (archivo):  # lee un archivo datos ordenado como sea, todos parametros, 
			# y los regresa como una lista, ya evaluados.


    var = []
    for line in file(archivo):
        line = line.split()	# separa la linea en sus componentes
                #  	    if (len(line)>0) :	# si la linea no esta vacia
        for col in range(len(line)) : 
            if line[col][0:1] != '#':  # cuando linea no empiece con comentario
                # ver si es un numero o un string
                try:			# trata de ver si pasa por numero
                    var0=float(line[col])  #no funciona con eval, decidir float
                except ValueError:
                    var0=line[col]   # no pasa, es string.
                var.append(var0) # agregar este elemento a la lista	
            else: break

    return var

################################################################################


