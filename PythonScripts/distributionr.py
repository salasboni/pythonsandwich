#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

def getx1y1(x):
	R=len(x);       
	x1=np.zeros(R+1)
	y1 = np.arange(0,1,1./R)
	vv=np.array(sorted(x,))#reverse=True))
	for i in range(R):
		x1[i+1]=vv[i]

	y1[0]=0; y1[R-1]=1   	
	return x1,y1

def plotcdf(x1,y1):	
	R=len(x1)     
	for i in range(R-2):
		pl.plot([x1[i],x1[i+1]],[y1[i],y1[i]],'k--')
		pl.plot([x1[i+1],x1[i+1]],  [y1[i],y1[i+1]],'k--')  



import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
#from scipy.stats import norm
import scipy.stats as stats



n1 = 200
n2 = 100

mean1 = [0,0]
mean2 = [2,2]

cov1 = [[.5,0],[0,.2]]
cov2 = [[.5,0],[0,.2]]
print "there are a total of %d obs in pop 1 and %d obs in pop 2" %(n1,n2)
np.random.seed()

z1 = np.random.multivariate_normal(mean1,cov1,n1)
z2 = np.random.multivariate_normal(mean2,cov2,n2)

obs=np.vstack([z1,z2])
tags = [1]*n1 + [2]*n2
tags=np.array(tags)

n_samples = n1+n2

#obs = (obs - np.mean(obs,axis=1))/np.std(obs,axis=1)

order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

classifiers = dict(
	knn=neighbors.KNeighborsClassifier(),
	logistic=linear_model.LogisticRegression(C=1e5),
	svm=svm.LinearSVC(C=1e5, loss='l1'),
	gnb=GaussianNB(),
	lda=LDA(),
	qda=QDA()
	)

print "These will be the classifiers used:"
print classifiers.keys()

numclass=len(classifiers.keys())
reptot = 100
rtot = np.zeros(shape=(reptot,numclass))
ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:.9 * n_samples]
	y_train = tags[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]
	y_test = tags[.9 * n_samples:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		rt[ii] = ((1./(len(X_test)))*(y_test != y_predtot).sum())
		ii += 1
		
	rtot[ik] = len(X_test)*rt
	ik += 1
	
aat=['r--', 'g--', 'b--', 'c--','m--','y--','k--']

fig = pl.figure()
x1=z1[:,0];y1=z1[:,1];
pl.plot(x1,y1,'xb');#pl.axis('equal');
x2=z2[:,0];y2=z2[:,1];
pl.plot(x2,y2,'or');#pl.axis('equal');
print rtot[0,:]

fig = pl.figure()


dx = np.arange(0,1,1./reptot)
#cdf = stats.norm.cdf
dx1=np.arange(0,1,1./reptot)
p1=min(float(n1)/n_samples,float(n2)/n_samples)
x=stats.norm.cdf(dx1, p1, np.sqrt(p1*(1-p1)))
#x=stats.norm.cdf(dx1, min(n1,n2), np.sqrt(n_samples*p1*(1-p1)))

#x = stats.norm.pdf(dx,.333,1)
#x = x / ((dx*x).sum())
#x = np.cumsum(dx*x)
pl.plot(dx1,x,'k--',linewidth = 5)
fig = pl.figure()
for ik in range(numclass):
	rto = rtot[:,ik]
	rto = np.array(sorted(rto,reverse=True))
	rto = rto/((dx*rto).sum())
	rto = np.cumsum(dx*rto)
	aa=aat[ik]
	pl.plot(dx,rto,aa,linewidth=2.0)
	

pl.show()
