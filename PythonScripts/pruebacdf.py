#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com



def getx1y1(x):
	R=len(x);       
	x1=np.zeros(R+1)
	y1 = np.arange(0,1,1./R)
	vv=np.array(sorted(x,))#reverse=True))
	for i in range(R):
		x1[i+1]=vv[i]

	y1[0]=0; y1[R-1]=1   	
	return x1,y1

def plotcdf(x1,y1):	
	R=len(x1)     
	for i in range(R-2):
		pl.plot([x1[i],x1[i+1]],[y1[i],y1[i]],'k--')
		pl.plot([x1[i+1],x1[i+1]],  [y1[i],y1[i+1]],'k--')   


import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
#from scipy.stats import norm
import scipy.stats as stats

np.random.seed()

x = np.random.normal(0,1,1000)
x1,y1=getx1y1(x)

#R=len(x1)     

fig = pl.figure()
#for i in range(R-2):
#	pl.plot([x1[i],x1[i+1]],[y1[i],y1[i]],'k--')
#        pl.plot([x1[i+1],x1[i+1]],  [y1[i],y1[i+1]],'k--')        	

plotcdf(x1,y1)
pl.show()

