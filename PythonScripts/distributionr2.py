#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com
	
def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx

def vert_dist(xarray,array,xnulldist, nulldist, alpha):
	idxa = find_nearest(xarray,alpha)
	idxnull = find_nearest(xnulldist,alpha)
	vdist = array[idxa]-nulldist[idxnull]
	return vdist
		
def makedatagau(n1,n2):
	mean1 = [0,0];mean2 = [0,0]
	cov1 = [[.3,0],[0,.3]];cov2 = [[.3,0],[0,.3]]
	np.random.seed()
	z1 = np.random.multivariate_normal(mean1,cov1,n1)
	z2 = np.random.multivariate_normal(mean2,cov2,n2)
	return z1,z2
	
def kuipertest(x1,x2):
	kutest=ks_2samp(x1,x2)+ks_2samp(-x1,-x2)
	kutest = np.sqrt(len(x1))*kutest
	return kutest

def makedatamoon():
	moons =datasets.make_moons(600,0,.3)
	z=moons[0];y=moons[1]
	z1=z[y==0];z2=z[y==1];
	return z1,z2

def makedatacircle():
	circs=datasets.make_circles(600,0,.1)
	z=circs[0];y=circs[1]
	z1=z[y==0];z2=z[y==1];
	return z1,z2

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp

colors = ['r', 'g', 'b', 'c','m','y','k']
n1 = 300
n2 = 300
#z1,z2 = makedatagau(n1,n2)
#z1,z2 = makedatamoon()
z1,z2 = makedatacircle()

n1=len(z1)
n2=len(z2)
print "There are a total of %d obs in pop 1 and %d obs in pop 2" %(n1,n2)


fig = pl.figure()
x1 = z1[:,0];y1=z1[:,1];
pl.plot(x1,y1,'xb');#pl.axis('equal');
x2 = z2[:,0];y2=z2[:,1];
pl.plot(x2,y2,'or');#pl.axis('equal');


#iris = datasets.load_iris()
#X = iris.data#[:, :2]  # we only take the first two features.
#print len(X)
#Y = iris.target
#print Y
#Y = Y[:50]
#X = X[:50,:]
#h = .02  # step size in the mesh



#####z12 = np.random.uniform(0,1,n2).T
#####z22 = np.random.uniform(0,2,n2).T
####z2 = np.vstack([z12,z22]).T
#n1=len(x1);n2=len(x2)

obs = np.vstack([z1,z2])
tags = [1]*n1 + [2]*n2

#obs = X
#tags = Y
tags=np.array(tags)

n_samples = len(obs)#n1+n2

#obs = (obs - np.mean(obs,axis=1))/np.std(obs,axis=1)

order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

#print tags

classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	logistic = linear_model.LogisticRegression(C=1e5),
	svm = svm.LinearSVC(),#(C=1e5, loss='l1'),
	svm_rbf = svm.SVC(kernel= 'rbf'),#(C=1e5, loss='l1'),
	#svm_ploy=svm.SVC(kernel='poly'),
	#gnb = GaussianNB(),
	lda = LDA(),
	qda = QDA()
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000

fig = pl.figure()
p1 = min(n1*1./n_samples,n2*1./n_samples)
std1 = np.sqrt(p1*(1-p1))/np.sqrt(.1*n_samples)
x = np.random.normal(p1,std1,reptot) #
sorted = np.sort(x)
pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),'k--',linewidth=3.0,label='gaussian approx')
x = np.random.binomial(.1*n_samples,p1,reptot) #stats.norm.cdf(dx1, p1, std1))
x = x*(1./(.1*n_samples))
sorted = np.sort(x)
pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[6],linewidth=3.0,label='binomial dist')
pl.legend(loc=1)

aa=np.array([0,0.0010,0.0030,0.0040,0.0070,0.0160,0.0290,0.0570,0.0980,0.1380,0.2130,0.2840,0.3520,0.4490,0.5600, 0.6520,0.7360,0.8080,0.8660,0.9150,0.9460,0.9730,0.9870,0.993,0.9960,0.9970,1.0000])
bb=np.array([0.0002,0.0002, 0.0013, 0.0031,0.0067,0.0137,0.0259,0.0462,0.0775,0.1225,0.1831, 0.2595, 0.3494, 0.4487, 0.5513, 0.6506,0.7405,0.8169, 0.8775,0.9225,0.9538,0.9741,0.9863,0.9933, 0.9969,0.9987,0.9995])
fig = pl.figure()
ax = fig.add_subplot(111)
# plotting distribution of p-values of binomial distribution
ax.step(aa,bb,'k--',linewidth=3.0,label='binomial dist')
#x=aa


rtot = np.zeros(shape=(reptot,numclass))
ptot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	pt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:.9 * n_samples]
	y_train = tags[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]
	y_test = tags[.9 * n_samples:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		rt[ii] = ((1./(len(X_test)))*(y_test != y_predtot).sum())
		pt[ii] = len(x[x<rt[ii]])*1./reptot
		ii += 1
		
	rtot[ik] = rt
	ptot[ik] = pt
	ik += 1

#fig = pl.figure()
#sorted=np.sort(x)
#pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[6],linewidth=2.0)

for ik in range(numclass):
	x1 = rtot[:,ik]
	sorted = np.sort(x1)
	pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	
	
#fig = pl.figure()
xa = np.arange(0,1,1./(.1*n_samples))
y = xa
value = 0.5
print len(xa)


fig = pl.figure()
aa=np.array([0,0.0010,0.0030,0.0040,0.0070,0.0160,0.0290,0.0570,0.0980,0.1380,0.2130,0.2840,0.3520,0.4490,0.5600, 0.6520,0.7360,0.8080,0.8660,0.9150,0.9460,0.9730,0.9870,0.993,0.9960,0.9970,1.0000])
bb=np.array([0.0002,0.0002, 0.0013, 0.0031,0.0067,0.0137,0.0259,0.0462,0.0775,0.1225,0.1831, 0.2595, 0.3494, 0.4487, 0.5513, 0.6506,0.7405,0.8169, 0.8775,0.9225,0.9538,0.9741,0.9863,0.9933, 0.9969,0.9987,0.9995])
ax = fig.add_subplot(111)
# plotting distribution of p-values of binomial distribution
ax.step(aa,bb,'k--',linewidth=3.0,label='binomial dist')

#ax = fig.add_subplot(111)
# plotting distribution of p-values of binomial distribution
#ax.step(xa,y,'k--',linewidth=3.0,label='binomial dist')

for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	#print sorted
	ysorted =np.arange( len(sorted)*1.0)/len(sorted)
	#print ysorted
	#print y

	# horizonal distance at P=.5
	#inp5 = find_nearest(ysorted, value)
	#pdist5 = .5 - sorted[inp5]
	ks = ks_2samp(xa,sorted)
	
	print "the kolmogorov smirnov stat using %s is %f" % (mylegend[ik],ks[0])#pdist5)
	# vertical distance at alpha
	#alpha = .01
	#vdist = vert_dist(sorted,ysorted,xa,y, alpha)
	#print "the vertical distance at alpha = %f, using %s is %f \n" % (alpha,mylegend[ik],vdist)
	pl.plot( sorted, ysorted ,colors[ik],label=mylegend[ik] )
	pl.legend(loc=4)


# Computing AUC of the difference of both cdfs
for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	lastx = sorted[-1]
	x1 = sorted[1:]-sorted[:-1]
	x2 = np.arange( len(sorted)*1.0)/len(sorted)
	x2=x2[:-1]
	xa=x1*x2;	
	aou = xa.sum() + 1 - lastx - .5
	print "the AUC using %s is %f" % (mylegend[ik],aou)
	ik += 1


#fig = pl.figure()
#ax = fig.add_subplot(111)

#for ik in range(numclass):
#	a=230+ik+1
#	pl.subplot(a)
#	x = ptot[:,ik]
#	n, bins, patches = pl.hist(x, 20, normed=1, facecolor='green', alpha=0.75)

pl.show()
