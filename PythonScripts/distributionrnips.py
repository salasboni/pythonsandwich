#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com
	
def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx

def vert_dist(xarray,array,xnulldist, nulldist, alpha):
	idxa = find_nearest(xarray,alpha)
	idxnull = find_nearest(xnulldist,alpha)
	vdist = array[idxa]-nulldist[idxnull]
	return vdist
		
def makedatagau(n1,n2):
	mean1 = [0,0];mean2 = [0,0]
	cov1 = [[1,0],[0,1]];
	cov2 = [[1,0],[0,1]]
	#cov2 = [[.002,0],[0,5]]
	np.random.seed()
	z1 = np.random.multivariate_normal(mean1,cov1,n1)
	z2 = np.random.multivariate_normal(mean2,cov2,n2)
	return z1,z2
	
def kuipertest(x1,x2):
	kutest=ks_2samp(x1,x2)+ks_2samp(-x1,-x2)
	kutest = np.sqrt(len(x1))*kutest
	return kutest

def makedatamoon():
	moons =datasets.make_moons(400,0,.3)
	z=moons[0];y=moons[1]
	z1=z[y==0];z2=z[y==1];z2=z2[0:200]
	return z1,z2

def makedatacircle():
	circs=datasets.make_circles(500,1,.1)
	z=circs[0];y=circs[1]
	z1=z[y==0];z2=z[y==1];
	z1=z1[0:400]
	z2=z2[0:200]
	return z1,z2

def chi2gof(sorted,n,p,reptot):
	x=range(int(n))
	aa = binom.pmf(x,n,p)
	ex= reptot*aa
	indx = np.arange(0,n,1)
	newsteps = binom.cdf(indx,n,p)
	counts=np.zeros(shape=(len(ex),1))
	for val in sorted:
		for i in range(len(indx)-1):
			inx0=newsteps[i];inx1=newsteps[i+1];
			if((val>=inx0)&(val<inx1)):
				counts[i]=counts[i]+1	
#	print "observed"
#	print counts.T
#	print "Expected"
#	print ex#newsamp
	return ex,counts.T	
	
#	newex=[];ind=[]
#	i=0
#	while i<(len(ex)-1):		
#		aba=ex[i];
#		if aba>=4.8:
#			newex.append(aba);i=i+1;
#		else:
#			jk=1
#			while (aba<4.8)&((i+jk)<len(x)):
#				aba=aba+ex[i+jk];jk=jk+1
#			i=i+jk	
#			newex.append(aba);
#		ind.append(i)
#	newsamp = np.array(newex)
	#newsamp[-2]=newsamp[-1]+newsamp[-2];
#	newsamp = newsamp[:-1]
#	indx=np.array(ind)
#	newsteps = binom.cdf(indx,n,p)
	#newsteps=vstack([0,newsteps])
	#x = np.random.binomial(n,p,reptot) #stats.norm.cdf(dx1, p1, std1))
	#sorted = np.sort(x)                        
	#sorted = binom.cdf(sorted, n,p)
#	counts=np.zeros(shape=(len(indx)-1,1))
#	j=0
#	for val in sorted:
#		for i in range(len(indx)-1):
#			inx0=newsteps[i];inx1=newsteps[i+1];
#			if((val>=inx0)&(val<inx1)):
#				counts[i]=counts[i]+1
#	deg = len(counts)-1
		
	#print chisq
#	if counts.sum()==0:
#		counts[0]=reptot
#	if counts.sum()==0:
#		counts[0]=1
#	else:
#		counts = counts/(counts.sum())
#	newsamp = newsamp/(newsamp.sum())
	
#	chisq = (((counts - newsamp)**2))
	#print chisq
	#print newsamp
# 	chisq=chisq/newsamp
	#print chisq
#	return chisq,deg
	

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp, binom, mannwhitneyu, chisquare

colors = ['r', 'g', 'b', 'c','m','y','k']
n1 = 200
n2 = 400
z1,z2 = makedatagau(n1,n2)
#z1,z2 = makedatamoon()
#z1,z2 = makedatacircle()

n1=len(z1)
n2=len(z2)
print "There are a total of %d obs in pop 1 and %d obs in pop 2" %(n1,n2)


fig = pl.figure()
x1 = z1[:,0];y1=z1[:,1];
pl.plot(x1,y1,'xb');#pl.axis('equal');
x2 = z2[:,0];y2=z2[:,1];
pl.plot(x2,y2,'or');#pl.axis('equal');

obs = np.vstack([z1,z2])
tags = [0]*n1 + [1]*n2
tags=np.array(tags)
print tags
print type(tags)
n_samples = len(obs)#n1+n2

#obs = (obs - np.mean(obs,axis=1))/np.std(obs,axis=1)

order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

#print tags

classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	#logistic = linear_model.LogisticRegression(C=1e5),
	#svm = svm.LinearSVC(),#(C=1e5, loss='l1'),
	#svm_rbf = svm.SVC(kernel= 'rbf'),#(C=1e5, loss='l1'),
	#svm_ploy=svm.SVC(kernel='poly'),
	#gnb = GaussianNB(),
	lda = LDA(),
	#qda = QDA()
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000
p1 = min(n1*1./n_samples,n2*1./n_samples)
print p1
prtest=.1;
prtrain = 1 - prtest;
########

rtot = np.zeros(shape=(reptot,numclass))
ptot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	pt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:prtrain * n_samples]
	y_train = tags[:prtrain * n_samples]
	X_test = obs[prtrain * n_samples:]
	y_test = tags[prtrain * n_samples:]
	#print len((y_test[y_test>1]))
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		#y_predtot = np.random.binomial(1,p1,len(X_test))
		#y_predtot = np.ceil(2*np.random.uniform(0,1,len(X_test)))
		rt[ii] = (1./(len(X_test)))*((y_test != y_predtot).sum())
		#pt[ii] = len(xx[xx<rt[ii]])*1./reptot
		ii += 1
	
	rtot[ik] = rt
	ptot[ik] = binom.cdf(prtest*n_samples*rt, prtest*n_samples,p1)#pt
	ik += 1

x = (1./(prtest*n_samples))*np.random.binomial(prtest*n_samples,p1,reptot) #stats.norm.cdf(dx1, p1, std1))
aa = np.sort(x)   
aa1 = np.arange( len(aa)*1.0)/len(aa)

fig = pl.figure()
ax = fig.add_subplot(111)
ax.plot(aa,aa1,'k--',linewidth=3.0,label='binomial dist')

ik=0
for ik in range(numclass):
	x1 = rtot[:,ik]
	sorted = np.sort(x1)
	pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	

fig = pl.figure()
ax = fig.add_subplot(111)

#x = range(int(prtest*n_samples))
#sorted = np.sort(x)
#aa = binom.cdf(sorted, prtest*n_samples,p1)
#ab=np.hstack([[0],aa]);ab=ab[:-1];
#ax.step(aa,ab,'k--',linewidth=3.0,label='binomial dist')


x = np.random.binomial(prtest*n_samples,p1,reptot) #stats.norm.cdf(dx1, p1, std1))
sorted = np.sort(x)                        
aa = binom.cdf(sorted, prtest*n_samples,p1)
bb = np.arange( len(aa)*1.0)/len(aa)
ax.step(aa,bb,'k--',linewidth=3.0,label='binomial dist')

for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	ysorted =np.arange( len(sorted)*1.0)/len(sorted)
	lastx = sorted[-1]
	x1 = sorted[1:]-sorted[:-1];x2 = ysorted
	x2=x2[:-1]; xa=x1*x2;	
	ks,pks = ks_2samp(aa,sorted)	
	u, pu = mannwhitneyu(aa,sorted)
	u1 = u/(len(aa)*len(sorted))
	aou = abs(xa.sum() + 1 - lastx - .5)
	#au2 = aou/(len(aa)*len(sorted))
	print "The area under the curves is %.3f" % aou
	print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],ks,pks)
	print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],u,pu)
	print u1

	ex,counts=chi2gof(sorted,prtest*n_samples,prtest,reptot)
	print ex
	print counts
	#print chisq.sum()
	#print deg
	#print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],deg,chisq.sum())
	pl.plot( sorted, ysorted ,colors[ik],label=mylegend[ik] )
	pl.legend(loc=4)

pl.show()
