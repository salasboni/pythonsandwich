#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com
	
def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx

def vert_dist(xarray,array,xnulldist, nulldist, alpha):
	idxa = find_nearest(xarray,alpha)
	idxnull = find_nearest(xnulldist,alpha)
	vdist = array[idxa]-nulldist[idxnull]
	return vdist
		
def makedatagau(n1,n2):
	mean1 = [0,0];mean2 = [0,0]
	cov1 = [[.3,0],[0,.3]];cov2 = [[.3,0],[0,.3]]
	np.random.seed()
	z1 = np.random.multivariate_normal(mean1,cov1,n1)
	z2 = np.random.multivariate_normal(mean2,cov2,n2)
	return z1,z2
	
def kuipertest(x1,x2):
	kutest=ks_2samp(x1,x2)+ks_2samp(-x1,-x2)
	kutest = np.sqrt(len(x1))*kutest
	return kutest

def makedatamoon():
	moons =datasets.make_moons(600,0,.3)
	z=moons[0];y=moons[1]
	z1=z[y==0];z2=z[y==1];
	return z1,z2

def makedatacircle():
	circs=datasets.make_circles(600,1,.1)
	z=circs[0];y=circs[1]
	z1=z[y==0];z2=z[y==1];
	return z1,z2




import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp, binom

colors = ['r', 'g', 'b', 'c','m','y','k']
n1 = 300
n2 = 300
z1,z2 = makedatagau(n1,n2)
#z1,z2 = makedatamoon()
#z1,z2 = makedatacircle()

n1=len(z1)
n2=len(z2)
print "There are a total of %d obs in pop 1 and %d obs in pop 2" %(n1,n2)


fig = pl.figure()
x1 = z1[:,0];y1=z1[:,1];
pl.plot(x1,y1,'xb');#pl.axis('equal');
x2 = z2[:,0];y2=z2[:,1];
pl.plot(x2,y2,'or');#pl.axis('equal');

obs = np.vstack([z1,z2])
tags = [1]*n1 + [2]*n2
tags=np.array(tags)

n_samples = len(obs)#n1+n2

#obs = (obs - np.mean(obs,axis=1))/np.std(obs,axis=1)

order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

#print tags

classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	logistic = linear_model.LogisticRegression(C=1e5),
	svm = svm.LinearSVC(),#(C=1e5, loss='l1'),
	svm_rbf = svm.SVC(kernel= 'rbf'),#(C=1e5, loss='l1'),
	#svm_ploy=svm.SVC(kernel='poly'),
	#gnb = GaussianNB(),
	lda = LDA(),
	qda = QDA()
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000
p1 = min(n1*1./n_samples,n2*1./n_samples)
prtest=.2;
prtrain = 1 - prtest;
########

	

fig = pl.figure()
ax = fig.add_subplot(111)

x = np.random.binomial(prtest*n_samples,p1,reptot) #stats.norm.cdf(dx1, p1, std1))
print prtest*n_samples
#x=range(int(prtest*n_samples))
#print x
sorted = np.sort(x)

aa = binom.cdf(sorted, prtest*n_samples,p1)
#print aa
bb = np.arange( len(aa)*1.0)/len(aa)
#print np.unique(aa)
print len(np.unique(aa))
#print bb
ax.step(aa,bb,'k--',linewidth=3.0,label='binomial dist monte calro')


x = range(int(prtest*n_samples))

sorted = np.sort(x)
print len(sorted)
aa = binom.cdf(sorted, prtest*n_samples,p1)
#ab=aa;
ab=np.hstack([[0],aa])
ab=ab[:-1];
print ab
#print aa
ax.step(aa,ab,'r--',linewidth=3.0,label='binomial dist real')
ax.plot(bb,bb,'b--')

pl.show()
