#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA

numclass = 4 #num of classifiers

mutfile = "/Users/newuser/Documents/newestdata/EXT_iav-GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
contrfile = "/Users/newuser/Documents/newestdata/pBDPGAL4U@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"

mut = open(mutfile,"rt")
contr = open(contrfile,"rt")

ctmutturn = 0
ctmutnoturn = 0
mutants = []
for line in mut:
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctmutnoturn += 1
	else:
		ctmutturn += 1
		mutants.append(row)

ctconturn = 0
ctconnoturn = 0
controls = []	
for line in contr:	
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctmutnoturn += 1
	else:
		ctmutturn += 1
		controls.append(row)
		
# for normal datasets, use commented code below to load data instead
#mutants = []
#for line in mut:
#	row = line.split()
#	row = [float(i) for i in row]		
#	mutants.append(row)
#
#controls = []	
#for line in contr:	
#	row = line.split()
#	row = [float(i) for i in row]	
#	controls.append(row)				


print len(controls)
controls = controls[0:300]

lenmu = len(mutants)
lencon = len(controls)

obs = mutants
obs.extend(controls)
obs=np.array(obs)

tags = [1]*lenmu + [2]*lencon
tags=np.array(tags)

m=2
# Getting rid of outliers and normalizing the data
#obs = [abs(obs - np.mean(obs,axis=0)) < m * np.std(obs,axis=0)]
obs = (obs - np.mean(obs,axis=0))/np.std(obs,axis=0)



#print tags 
#print type(tags)
#print len(tags)
#print len(mutants)
#print len(controls)
#print len(obs)

n_samples = len(obs)
print "a total of %d observations" % n_samples

np.random.seed(0)
order = np.random.permutation(n_samples)
#print order
obs = obs[order]
tags = tags[order].astype(np.float)
#print "these are the tags"
#print tags


	
n_samples = len(obs);
#obs=obs[:,0:4]
print obs.shape
print "the number of obs is: %d" % len(obs)
knn = neighbors.KNeighborsClassifier()
X_train = obs[:.9 * n_samples]
y_train = tags[:.9 * n_samples]
X_test = obs[.9 * n_samples:]
y_test = tags[.9 * n_samples:]


acca=knn.fit(X_train,y_train).predict(X_test)
acca1=(y_test == acca).sum()
#y_predtot=KNeighborsClassifier.fit(obs,tags).predict(obs)
#acca= (100/n_samples)*(tags == y_predtot).sum()
print "this should work: %d" % acca1

classifiers = dict(
	knn=neighbors.KNeighborsClassifier(),
	logistic=linear_model.LogisticRegression(C=1e5),
	svm=svm.LinearSVC(C=1e5, loss='l1'),
	#gnb=GaussianNB(),
		lda=LDA()
	)

print classifiers.keys()

ii = 0
numclass=len(classifiers.keys())
#acc = np.zeros(numclass)
#acccvtrue = np.zeros(numclass)
reptot = 100
acccvtrue = np.zeros(shape=(reptot,numclass))
ik = 0
for ik in range(reptot):
	acccv=np.zeros(numclass)
	ii = 0
	for name,clf in classifiers.iteritems():	
	#y_predtot = clf.fit(obs,tags).predict(obs)
	#acc[ii] = (100./(len(obs)))*(tags == y_predtot).sum()
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		acccv[ii] = (100./(len(X_test)))*(y_test == y_predtot).sum()
		ii += 1
		
	acccvtrue[ik] = acccv
	ik += 1
	
acctruemed = np.median(acccvtrue,axis=0)
print acctruemed

reptot = 10000
#acctot =  np.zeros(shape=(reptot,numclass))
acccvtot = np.zeros(shape=(reptot,numclass))
ik = 0

for ik in range(reptot):
	order = np.random.permutation(n_samples)
	tags = tags[order]
	y_train = tags[:.9 * n_samples]
	y_test = tags[.9 * n_samples:]
	#acc = np.zeros(numclass)
	acccv = np.zeros(numclass)

	ii = 0	
	for name, clf in classifiers.iteritems():	
		#y_predtot = clf.fit(obs,tags).predict(obs)
		#acc[ii] = (100./(len(obs)))*(tags == y_predtot).sum()
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		acccv[ii] = (100./(len(X_test)))*(y_test == y_predtot).sum()
		ii += 1
	
	#acctot[ik] = acc
	acccvtot[ik] = acccv
	ik += 1

print classifiers.keys()
#print np.mean(acctot,axis = 0)
#print np.std(acctot,axis = 0)

print np.mean(acccvtot,axis = 0)
print np.std(acccvtot,axis = 0)
medians = np.median(acccvtot,axis=0)
mutfile = mutfile.split('@',1)
mutfile = mutfile[0]
mutfile = mutfile.split('/')
mutfile = mutfile[-1]

mutfile = mutfile + '_acccv.txt'

np.savetxt(mutfile,acccvtot)

#pl.subplot(2,1,1)
#pl.boxplot(acctot)

#pl.subplot(2,1,2)
pl.figure()
pl.boxplot(acccvtot,notch=1,usermedians=medians,
)
for i in range(4):
	pl.plot(i+1, acctruemed[i],
        color='w', marker='*', markeredgecolor='k')

pl.show()





