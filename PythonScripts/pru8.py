#! /usr/bin/env python

#para leer un archivo con datos estilo sm
#

def leetabla(archivo,columna):  # lee un archivo datos ordenado por columnas
   var = []

   for line in file(archivo):
  	line = line.split()
  	if (len(line)>0) :	# si la linea no esta vacia

	  if line[0][0:1] != '#':  # cuando linea no empiece con comentario
					
	     # ver si es un numero o un string

#	     if line[columna-1].replace('.','0').replace('-','0').replace('e','0').replace('+','0').replace('E','0').isdigit():  #validar el numero
#			var0=eval(line[columna-1])
#	     else:
#			var0=line[columna-1]

	     try:			# trata de ver si pasa por numero
        	var0=float(line[columna-1])  #no funciona con eval, decidir float
    	     except ValueError:
        	var0=line[columna-1]   # no pasa, es string.

       	     var.append(var0) # agregar este elemento a la lista	

   return array(var) # convertir la lista a arreglo

def leetablav(archivo,columna):  # lee un archivo datos ordenado por columnas, 
				  #    varias columnas a la vez
   var = []

   for line in file(archivo):
  	line = line.split()
  	if (len(line)>0) :	# si la linea no esta vacia

	  if line[0][0:1] != '#':  # cuando linea no empiece con comentario
					
	     # ver si es un numero o un string

	     try:			# trata de ver si pasa por numero
        	var0=float(line[columna-1])  #no funciona con eval, decidir float
    	     except ValueError:
        	var0=line[columna-1]   # no pasa, es string.

       	     var.append(var0) # agregar este elemento a la lista	

   return array(var) # convertir la lista a arreglo



from numpy import*
import sys

print sys.version

data = 'kkposlaser.dat'
x ,y, nom = leetabla( data, 1), leetabla( data, 5), leetabla( data, 7)

#x,y = leetablav(data, (1,5) )
#x=2.123+x

y = y+0
indices = argsort(y);  #  ordena las y's

for i in range(len(x)):
  print '10%s %5d  %5.3f \n' , nom[i],x[i],y[i],indices[i]

print "suma de x's", x.sum()



import scipy 
import sys

print sys.version

polycoeffs = scipy.polyfit(x, y, 2) # ajusta polinomio de grado ...
print polycoeffs
yfit = scipy.polyval(polycoeffs, x)


import matplotlib.pyplot as plt

plt.plot(x,y,'D')
plt.plot(x,yfit,'g')
plt.show()
