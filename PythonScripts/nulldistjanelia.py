#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx

def vert_dist(xarray,array,xnulldist, nulldist, alpha):
	idxa = find_nearest(xarray,alpha)
	idxnull = find_nearest(xnulldist,alpha)
	vdist = array[idxa]-nulldist[idxnull]
	return vdist
	
import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
import scipy.stats as stats



########## Uncomment this section if you want to load filenames from the script. Make changes to path.
# Uncomment only one of the mutfile options.
#mutfile = "/Users/newuser/Documents/newestdata/GMR_11F05_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_20C06_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/EXT_iav-GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_61D08_AD_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_38A10_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
mutfile = "/Users/newuser/Documents/newestdata/MZZ_ppk1d9GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
contrfile = "/Users/newuser/Documents/newestdata/pBDPGAL4U@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"


#######Uncomment this section if you want the user to enter the file names for mutants and wild type
#print "Enter the path and name of mutant file: "
#mutfile = raw_input()
#print = "Enter the path and name of wildtype file: "
#contrfile = raw_input()

#This section reads the files. We extracr in columns corresponding to hunches, 7,8 and 9.
# Also, substitute the "-10" for "NA". In that case, convert fo float in the if loop, before appending.

mut = open(mutfile,"rt")
contr = open(contrfile,"rt")

ctmutturn = 0
ctmutnoturn = 0
mutants = []
for line in mut:
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctmutnoturn += 1
	else:
		ctmutturn += 1
		mutants.append(row)

ctconturn = 0
ctconnoturn = 0
controls = []	
for line in contr:	
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctconnoturn += 1
	else:
		ctconturn += 1
		controls.append(row)
		
##### for normal datasets, where you are not taking away columns or removing observations based on missing features, 
# use commented code below to load data instead
#mutants = []
#for line in mut:
#	row = line.split()
#	row = [float(i) for i in row]		
#	mutants.append(row)
#
#controls = []	
#for line in contr:	
#	row = line.split()
#	row = [float(i) for i in row]	
#	controls.append(row)				

lenmu = len(mutants)
lencon = len(controls)
print "there are a total of %d mutants and %d controls" %(lenmu,lencon)

propmut = float(ctmutturn)/(ctmutturn + ctmutnoturn)
propcont = float(ctconturn)/(ctconturn + ctconnoturn)
newcon = int((ctmutturn + ctmutnoturn)*propcont)

print "the proportion of mutants that turn and controls that turn are %f and %f"%(propmut,propcont)
print "the new number of controls will be %d" % newcon

controls = controls[0:newcon]
lencon = newcon

obs = mutants
obs.extend(controls)
obs=np.array(obs)
n_samples = len(obs)

tags = [1]*lenmu + [2]*lencon
tags=np.array(tags)

##### Uncomment for getting rid of outliers
#Getting rid of outliers and normalizing the data
#m=2
#obs = [abs(obs - np.mean(obs,axis=0)) < m * np.std(obs,axis=0)]

# Normalizing the data
obs = (obs - np.mean(obs,axis=0))/np.std(obs,axis=0)

print "a total of %d observations, with %d controls and %d mutants" % (n_samples,newcon,lenmu)

np.random.seed(0)
order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)
colors=['r', 'g', 'b', 'c','m','y','k']
classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	logistic = linear_model.LogisticRegression(C=1e5),
	svm = svm.LinearSVC(),#C=1e5, loss='l1'),
	gnb = GaussianNB(),
	lda = LDA(),
	qda = QDA()
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000

fig = pl.figure()
p1 = min(lenmu*1./n_samples,lencon*1./n_samples)
std1 = np.sqrt(p1*(1-p1))/np.sqrt(.1*n_samples)
x = np.random.normal(p1,std1,reptot) #
sorted = np.sort(x)
pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),'k--',linewidth=3.0,label='gaussian approx')
x = np.random.binomial(.1*n_samples,p1,reptot) #stats.norm.cdf(dx1, p1, std1))
x = x*(1./(.1*n_samples))
sorted = np.sort(x)
pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[6],linewidth=3.0,label='binomial dist')
pl.legend(loc=1)

ptot = np.zeros(shape=(reptot,numclass))
rtot = np.zeros(shape=(reptot,numclass))
ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	pt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:.9 * n_samples]
	y_train = tags[:.9 * n_samples]
	X_test = obs[.9 * n_samples:]
	y_test = tags[.9 * n_samples:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		rt[ii] = ((1./(len(X_test)))*(y_test != y_predtot).sum())
		pt[ii] = len(x[x<rt[ii]])*1./reptot
		ii += 1
		
	rtot[ik] = rt
	ptot[ik] = pt
	ik += 1
	

for ik in range(numclass):
	x = rtot[:,ik]
	sorted = np.sort(x)
	ysorted = np.arange( len(sorted)*1.0)/len(sorted)
	pl.plot( sorted, ysorted ,colors[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	
	
fig = pl.figure()
xa = np.arange(0,1,1./(.1*n_samples))
y = xa
value = 0.5
ax = fig.add_subplot(111)
# plotting distribution of p-values of binomial distribution
ax.step(xa,y,'k--',linewidth=3.0,label='binomial dist')

for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	ysorted =np.arange( len(sorted)*1.0)/len(sorted)
	# horizonal distance at P=.5
	inp5 = find_nearest(ysorted, value)
	pdist5 = .5 - sorted[inp5]
	#print "the horizontal distance using %s is %f" % (mylegend[ik],pdist5)
	# vertical distance at alpha
	alpha = .01
	vdist = vert_dist(sorted,ysorted,xa,y, alpha)
	#print "the vertical distance at alpha = %f, using %s is %f \n" % (alpha,mylegend[ik],vdist)
	pl.plot( sorted, ysorted ,colors[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	lastx = sorted[-1]
	x1 = sorted[1:]-sorted[:-1]
	x2 = np.arange( len(sorted)*1.0)/len(sorted)
	x2=x2[:-1]
	xa1=x1*x2;	
	aou = xa1.sum() + 1 - lastx - 0.5
	#print "the AUC using %s is %f" % (mylegend[ik],aou)
	print "\mbox{ %s} & %.3f & %.3f & %.3f \\ \hline" % (mylegend[ik],aou,pdist5,vdist)


fig = pl.figure()
for ik in range(numclass):
	a=230+ik+1
	pl.subplot(a)
	x = rtot[:,ik]
	n, bins, patches = pl.hist(x, 20, normed=1, facecolor='red', alpha=0.75)

pl.show()
