#!/usr/bin/env ipython

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
import scipy.stats as stats
#moons=datasets.make_moons(300,0,.3)
#moons=datasets.make_blobs(200,2,3,2)
moons=datasets.make_circles(200,1,.1)

X=moons[0]
y=moons[1]
#print moons
X0 = X[y==0]
X1 = X[y==1]
X2 = X[y==2]

colors=['r', 'g']
fig=pl.figure()
pl.plot(X0[:,0],X0[:,1],'xb',X1[:,0],X1[:,1],'or')
pl.plot(X2[:,0],X2[:,1],'sg')
pl.show()


