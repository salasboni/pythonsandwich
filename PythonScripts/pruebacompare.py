#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA

#controls = load(datasetcontrols)
#mutants = load(datasetmutants)

#controlstags = zeros # ???
#mutantstags = ones #???

#obs = merge(controls, mutants) # CHECK THIS COMMAND
#tags = merge(controlstags, mutantstags)

#n_samples = len(obs);

#np.random.seed(0)
#order = np.random.permutation(n_sample)
#obs = obs[order]
#tags = tags[order]

#obs_train = obs_[:.9*n_samples]
#tags_train = tags_[:.9*n_samples]
#obs_test = obs_[.9*n_samples:]
#tags_test= tags_[.9*n_samples:]



iris = datasets.load_iris()
X_digits = iris.data[:,:2]
y_digits = iris.target

n_samples = len(X_digits)

np.random.seed(0)
order = np.random.permutation(n_samples)

X_digits = X_digits[order]
y_digits = y_digits[order]


X_train = X_digits[:.9 * n_samples]
y_train = y_digits[:.9 * n_samples]
X_test = X_digits[.9 * n_samples:]
y_test = y_digits[.9 * n_samples:]
print y_test
type(y_test)

# add lda and qda, maybe naive bayes!!!!
classifiers = dict(
    knn=neighbors.KNeighborsClassifier(),
    logistic=linear_model.LogisticRegression(C=1e5),
    svm=svm.LinearSVC(C=1e5, loss='l1'),
    gnb=GaussianNB(),
    lda=LDA()
    )

fignum = 1
for name, clf in classifiers.iteritems():
    y_pred=clf.fit(X_train, y_train).predict(X_test)
    
    #y_pred = clf.fit(X, Y).predict(X)
    print "Number of correcly labeled points using %s : %d" % (name,(y_test == y_pred).sum())


    fignum += 1

pl.show()



