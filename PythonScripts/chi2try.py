#!/usr/bin/env ipython


import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp, binom, mannwhitneyu

n1 = 300
n2 = 300
reptot = 1000
n_samples=n1+n2
p1 = min(n1*1./n_samples,n2*1./n_samples)
prtest=.1;
n=prtest*(n1+n2);
p=.5
x=range(int(n))
aa = binom.pmf(x,n,p)
ex= 1000*aa
print len(ex)
print ex
newex=[];ind=[]
i=0
while i<(len(ex)-1):		
	aba=ex[i];
	if aba>=5:
		newex.append(aba);i=i+1;
	else:
		jk=1
		while (aba<5)&((i+jk)<len(x)):
			aba=aba+ex[i+jk];jk=jk+1
		i=i+jk	
		newex.append(aba);
	ind.append(i)
	
newsamp = np.array(newex)
newsamp[-2]=newsamp[-1]+newsamp[-2];
newsamp = newsamp[:-1]
indx=np.array(ind)

newsteps = binom.cdf(indx,n,p)


x = np.random.binomial(n,p,reptot) #stats.norm.cdf(dx1, p1, std1))
sorted = np.sort(x)                        
sorted = binom.cdf(sorted, n,p)

counts=np.zeros(shape=(len(indx)-1,1))

j=0
for val in sorted:
	for i in range(len(indx)-1):
		inx0=newsteps[i];inx1=newsteps[i+1];
		if((val>=inx0)&(val<inx1)):
			counts[i]=counts[i]+1


print val			
print newsteps
print indx
print newsamp
print len(counts)
print len(newsamp)
print newsteps
print counts

