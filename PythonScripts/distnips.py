#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com
		
def makedatagau(n1,n2):
	mean1 = [0,0];mean2 = [0,0]
	cov1 = [[1,0],[0,1]];
	#cov2 = [[1,0],[0,1]]
	cov2 = [[1,0],[0,1]]
	np.random.seed()
	z1 = np.random.multivariate_normal(mean1,cov1,n1)
	z2 = np.random.multivariate_normal(mean2,cov2,n2)
	return z1,z2

def makedatamoon():
	moons =datasets.make_moons(800,0,.3)
	z=moons[0];y=moons[1]
	z1=z[y==0];z2=z[y==1];
	order = np.random.permutation(len(z2))
	z2=z2[order]
	z2=z2[0:200]
	return z1,z2

def makedatacircle():
	circs=datasets.make_circles(900,1,.1)
	z=circs[0];y=circs[1]
	z1=z[y==0];z2=z[y==1];
	order = np.random.permutation(len(z2))
	z2=z2[order];z2=z2[0:200]
	order = np.random.permutation(len(z1))
	z1=z1[order]
	z1=z1[0:400]
	return z1,z2

def chi2gof(sorted,n,p,reptot):
	x=range(int(n))
	aa = binom.pmf(x,n,p)
	ex= reptot*aa
	indx = np.arange(0,n,1)
	newsteps = binom.cdf(indx,n,p)
	counts=np.zeros(shape=(len(ex),1))
	for val in sorted:
		for i in range(len(indx)-1):
			inx0=newsteps[i];inx1=newsteps[i+1];
			if((val>=inx0)&(val<inx1)):
				counts[i]=counts[i]+1	

	return ex,counts.T	

def ks_2sampmod(data1, data2):
    """
    Computes the Kolmogorov-Smirnof statistic on 2 samples, with data1 over data2
    """
    
    data1, data2 = map(np.asarray, (data1, data2))
    n1 = data1.shape[0]
    n2 = data2.shape[0]
    n1 = len(data1)
    n2 = len(data2)
    data1 = np.sort(data1)
    data2 = np.sort(data2)
    data_all = np.concatenate([data1,data2])
    cdf1 = np.searchsorted(data1,data_all,side='right')/(1.0*n1)
    cdf2 = (np.searchsorted(data2,data_all,side='right'))/(1.0*n2)
    #d = np.max(np.absolute(cdf1-cdf2))
    Dplus = np.max((cdf1-cdf2))
    #if d<0:
    #	    d=0
    en = np.sqrt(n1*n2/float(n1+n2))
    #prob = ksprob((en+0.12+0.11/en)*d)
    return Dplus, distributions.ksone.sf(Dplus,n1)
    #try:
    #    prob = ksprob((en+0.12+0.11/en)*d)
    #except:
    #    prob = 1.0
    #return d, prob

def mannwhitneyumod(x, y, use_continuity=True):
    """
    Computes the Mann-Whitney rank test on samples x and y.

    Parameters
    ----------
    x, y : array_like
        Array of samples, should be one-dimensional.
    use_continuity : bool, optional
            Whether a continuity correction (1/2.) should be taken into
            account. Default is True.

    Returns
    -------
    u : float
        The Mann-Whitney statistics.
    prob : float
        One-sided p-value assuming a asymptotic normal distribution.

    Notes
    -----
    Use only when the number of observation in each sample is > 20 and
    you have 2 independent samples of ranks. Mann-Whitney U is
    significant if the u-obtained is LESS THAN or equal to the critical
    value of U.

    This test corrects for ties and by default uses a continuity correction.
    The reported p-value is for a one-sided hypothesis, to get the two-sided
    p-value multiply the returned p-value by 2.

    """
    x = asarray(x)
    y = asarray(y)
    n1 = len(x)
    n2 = len(y)
    ranked = rankdata(np.concatenate((x,y)))
    rankx = ranked[0:n1]       # get the x-ranks
    #ranky = ranked[n1:]        # the rest are y-ranks
    u1 = n1*n2 + (n1*(n1+1))/2.0 - np.sum(rankx,axis=0)  # calc U for x
    u2 = n1*n2 - u1                            # remainder is U for y
    bigu = max(u1,u2)
    smallu = min(u1,u2)
    #T = np.sqrt(tiecorrect(ranked))  # correction factor for tied scores
    T = tiecorrect(ranked)
    if T == 0:
        raise ValueError('All numbers are identical in amannwhitneyu')
    sd = np.sqrt(T*n1*n2*(n1+n2+1)/12.0)

    if use_continuity:
        # normal approximation for prob calc with continuity correction
        z = abs((bigu-0.5-n1*n2/2.0) / sd)
    else:
        z = abs((bigu-n1*n2/2.0) / sd)  # normal approximation for prob calc
    return smallu, distributions.norm.sf(z)  #(1.0 - zprob(z))


import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp, binom, mannwhitneyu, chisquare, ksprob, ksone, distributions

markers = ['o','x','d','*','^','s']
colors = ['r', 'g', 'b', 'c','m','y','k'];n1 = 200;n2 = 400
#z1,z2 = makedatagau(n1,n2)
#z1,z2 = makedatamoon()
z1,z2 = makedatacircle()

n1=len(z1);n2=len(z2)
print "There are a total of %d obs in pop 1 and %d obs in pop 2" %(n1,n2)


fig = pl.figure()
x1 = z1[:,0];y1=z1[:,1];
pl.plot(x1,y1,'xb');#pl.axis('equal');
x2 = z2[:,0];y2=z2[:,1];
pl.plot(x2,y2,'or');#pl.axis('equal');

obs = np.vstack([z1,z2])
tags = [0]*n1 + [1]*n2
tags=np.array(tags)

n_samples = len(obs)#n1+n2

#obs = (obs - np.mean(obs,axis=1))/np.std(obs,axis=1)

order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)

#print tags

classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	logistic = linear_model.LogisticRegression(C=1e5),
	svm = svm.LinearSVC(),#(C=1e5, loss='l1'),
	svm_rbf = svm.SVC(kernel= 'rbf'),#(C=1e5, loss='l1'),
	#svm_ploy=svm.SVC(kernel='poly'),
	#gnb = GaussianNB(),
	lda = LDA(),
	qda = QDA()
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000
pmin = min(n1*1./n_samples,n2*1./n_samples)
print pmin
prtest=.1;
prtrain = 1 - prtest;
ntr = n_samples*prtrain
nte = n_samples*prtest
########

rtot = np.zeros(shape=(reptot,numclass))
ptot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	pt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:ntr];y_train = tags[:ntr]
	X_test = obs[ntr:];y_test = tags[ntr:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		#y_predtot = np.random.binomial(1,p1,len(X_test))
		#y_predtot = np.ceil(2*np.random.uniform(0,1,len(X_test)))
		rt[ii] = (1./nte)*((y_test != y_predtot).sum())
		ii += 1
	
	rtot[ik] = rt
	ptot[ik] = binom.cdf(nte*rt, nte,pmin)#pt
	ik += 1

x = np.random.binomial(nte,pmin,reptot)
aa = (1./(nte))*np.sort(x)   
aa1 = np.arange( len(aa)*1.0)/len(aa)

fig = pl.figure()
ax = fig.add_subplot(111)
ax.plot(aa,aa1,'k--',linewidth=3.0,label='binomial dist')

ik=0
for ik in range(numclass):
	x1 = rtot[:,ik]
	sorted = np.sort(x1)
	pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[ik],marker=markers[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	

fig = pl.figure()
ax = fig.add_subplot(111)

#x = np.random.binomial(nte,pmin,reptot) #stats.norm.cdf(dx1, p1, std1))
sorted = np.sort(x)                        
aa = binom.cdf(sorted, nte,pmin)
bb = np.arange( len(aa)*1.0)/len(aa)
ax.step(aa,bb,'k--',linewidth=3.0,label='binomial dist')

#x = range(int(nte))
#sorted = np.sort(x)
#aa1 = binom.cdf(sorted,nte,pmin)
#ab=np.hstack([[0],aa1]);ab=ab[:-1];
#ax.step(aa1,ab,'r--',linewidth=3.0,label='binomial dist')
makeverys=[16,19,21,24,19,22]
for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	ysorted =np.arange( len(sorted)*1.0)/len(sorted)
	lastx = sorted[-1]
	x1 = sorted[1:]-sorted[:-1];x2 = ysorted
	x2=x2[:-1]; xa=x1*x2;	
	ks,pks = ks_2sampmod(sorted,aa)	
	print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],ks,pks)	
	#ks,pks = ks_2sampmod(sorted,aa1)	
	#u, pu = mannwhitneyu(sorted,aa)
	#u1 = u/(len(aa)*len(sorted))
	#aou = abs(xa.sum() + 1 - lastx - .5)
	#au2 = aou/(len(aa)*len(sorted))
	#print "The area under the curves is %.3f" % aou
	#print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],ks,pks)
	#print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],u,pu)
	pl.plot( sorted, ysorted ,colors[ik],marker=markers[ik],markevery=makeverys[ik],label=mylegend[ik] )
	pl.legend(loc=4)

pl.show()
