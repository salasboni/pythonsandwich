#!/usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

def find_nearest(array,value):
	idx = (np.abs(array-value)).argmin()
	return idx

def vert_dist(xarray,array,xnulldist, nulldist, alpha):
	idxa = find_nearest(xarray,alpha)
	idxnull = find_nearest(xnulldist,alpha)
	vdist = array[idxa]-nulldist[idxnull]
	return vdist
def ecdf(x):
  # normalize X to sum to 1
  x = x / np.sum(x)
  return np.cumsum(x)
def ks_2sampmod(data1, data2):
    """
    Computes the Kolmogorov-Smirnof statistic on 2 samples, with data1 over data2
    """
    
    data1, data2 = map(np.asarray, (data1, data2))
    n1 = data1.shape[0]
    n2 = data2.shape[0]
    n1 = len(data1)
    n2 = len(data2)
    data1 = np.sort(data1)
    data2 = np.sort(data2)
    data_all = np.concatenate([data1,data2])
    cdf1 = np.searchsorted(data1,data_all,side='right')/(1.0*n1)
    cdf2 = (np.searchsorted(data2,data_all,side='right'))/(1.0*n2)
    #d = np.max(np.absolute(cdf1-cdf2))
    d = np.max((cdf1-cdf2))
    if d<0:
    	    d=0
    #Note: d a signed distance
    en = np.sqrt(n1*n2/float(n1+n2))
    prob = ksprob((en+0.12+0.11/en)*d)

    #try:
    #    prob = ksprob((en+0.12+0.11/en)*d)
    #except:
    #    prob = 1.0
    return d, prob

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import random		
import matplotlib as mpl
from scipy.stats import kstest, ks_2samp, binom, mannwhitneyu, chisquare, ksprob, ttest_ind



########## Uncomment this section if you want to load filenames from the script. Make changes to path.
# Uncomment only one of the mutfile options.
mutfile = "/Users/newuser/Documents/newestdata/GMR_20C06_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/EXT_iav-GAL4@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
#mutfile = "/Users/newuser/Documents/newestdata/GMR_38A10_AE_01@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"
contrfile = "/Users/newuser/Documents/newestdata/pBDPGAL4U@UAS_Shi_ts1_3_0001@t4@b_50hz2V_30s1x30s0s#b_1000hz2V_90s1x30s0s#b_200hz2V_150s20x0STOTAL.txt"


#######Uncomment this section if you want the user to enter the file names for mutants and wild type
#print "Enter the path and name of mutant file: "
#mutfile = raw_input()
#print = "Enter the path and name of wildtype file: "
#contrfile = raw_input()

#This section reads the files. We extracr in columns corresponding to hunches, 7,8 and 9.
# Also, substitute the "-10" for "NA". In that case, convert fo float in the if loop, before appending.

mut = open(mutfile,"rt")
contr = open(contrfile,"rt")

ctmutturn = 0
ctmutnoturn = 0
mutants = []
for line in mut:
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctmutnoturn += 1
	else:
		ctmutturn += 1
		mutants.append(row)

ctconturn = 0
ctconnoturn = 0
controls = []	
for line in contr:	
	row = line.split()
	row = list(row[i] for i in [0,1,2,3,4,5,6,10,11])
	row = [float(i) for i in row]	
	rowi=row[4]
	if rowi ==-10:
		ctconnoturn += 1
	else:
		ctconturn += 1
		controls.append(row)
		
##### for normal datasets, where you are not taking away columns or removing observations based on missing features, 
# use commented code below to load data instead
#mutants = []
#for line in mut:
#	row = line.split()
#	row = [float(i) for i in row]		
#	mutants.append(row)
#
#controls = []	
#for line in contr:	
#	row = line.split()
#	row = [float(i) for i in row]	
#	controls.append(row)				

lenmu = len(mutants)
lencon = len(controls)
print "there are a total of %d mutants and %d controls" %(lenmu,lencon)

propmut = float(ctmutturn)/(ctmutturn + ctmutnoturn)
propcont = float(ctconturn)/(ctconturn + ctconnoturn)
newcon = int((ctmutturn + ctmutnoturn)*propcont)

print "the proportion of mutants that turn and controls that turn are %f and %f"%(propmut,propcont)
print "the new number of controls will be %d" % newcon

controls = controls[0:newcon]
lencon = newcon

obs = mutants
obs.extend(controls)
obs=np.array(obs)
n_samples = len(obs)

tags = [1]*lenmu + [2]*lencon
tags=np.array(tags)

##### Uncomment for getting rid of outliers
#Getting rid of outliers and normalizing the data
#m=2
#obs = [abs(obs - np.mean(obs,axis=0)) < m * np.std(obs,axis=0)]

# Normalizing the data
obs = (obs - np.mean(obs,axis=0))/np.std(obs,axis=0)
print ttest_ind(controls,mutants)


print "a total of %d observations, with %d controls and %d mutants" % (n_samples,newcon,lenmu)

np.random.seed(0)
order = np.random.permutation(n_samples)
obs = obs[order]
tags = tags[order].astype(np.float)
colors=['r', 'g', 'b', 'c','m','y','k']
markers = ['o','x','d','*','^','s']

classifiers = dict(
	knn = neighbors.KNeighborsClassifier(),
	logistic = linear_model.LogisticRegression(C=1e5),
	svm = svm.LinearSVC(),#(C=1e5, loss='l1'),
	svm_rbf = svm.SVC(kernel= 'rbf'),#(C=1e5, loss='l1'),
	svm_ploy=svm.SVC(kernel='poly'),
	#gnb = GaussianNB(),
	lda = LDA(),
	#qda = QDA()
	)

print "These will be the classifiers used:"
mylegend = classifiers.keys()
print mylegend
numclass = len(classifiers.keys())
reptot = 1000
p1 = min(lenmu*1./n_samples,lencon*1./n_samples)
prtest=.2;
prtrain = 1 - prtest;
########

rtot = np.zeros(shape=(reptot,numclass))
ptot = np.zeros(shape=(reptot,numclass))

ik = 0

for ik in range(reptot):
	rt=np.zeros(numclass)
	pt=np.zeros(numclass)
	order = np.random.permutation(n_samples)
	obs = obs[order]
	tags = tags[order]
	X_train = obs[:prtrain * n_samples]
	y_train = tags[:prtrain * n_samples]
	X_test = obs[prtrain * n_samples:]
	y_test = tags[prtrain * n_samples:]
	ii = 0
	for name,clf in classifiers.iteritems():	
		y_predtot = clf.fit(X_train, y_train).predict(X_test)
		rt[ii] = ((1./(len(X_test)))*(y_test != y_predtot).sum())
		#pt[ii] = len(xx[xx<rt[ii]])*1./reptot
		ii += 1
		
	rtot[ik] = rt
	ptot[ik] = binom.cdf(prtest*n_samples*rt, prtest*n_samples,p1)#pt
	ik += 1


fig = pl.figure()
ax = fig.add_subplot(111)
ik=0
for ik in range(numclass):
	x1 = rtot[:,ik]
	sorted = np.sort(x1)
	pl.plot( sorted, np.arange( len(sorted)*1.0)/len(sorted),colors[ik],marker=markers[ik],label=mylegend[ik] )
	pl.legend(loc=4)
	

fig = pl.figure()
ax = fig.add_subplot(111)

x = np.random.binomial(prtest*n_samples,p1,reptot) #stats.norm.cdf(dx1, p1, std1))
sorted = np.sort(x)
aa = binom.cdf(sorted, prtest*n_samples,p1)
bb = np.arange( len(aa)*1.0)/len(aa)

ax.step(aa,bb,'k--',linewidth=3.0,label='binomial dist')



#marker='o', linestyle='--',
makeverys=[16,19,21,24,19,22]

for ik in range(numclass):
	x1 = ptot[:,ik]
	sorted = np.sort(x1)
	ysorted =np.arange( len(sorted)*1.0)/len(sorted)
	lastx = sorted[-1]
	x1 = sorted[1:]-sorted[:-1];x2 = ysorted
	x2=x2[:-1]; xa=x1*x2;
	ks,pks = ks_2sampmod(sorted,aa)	
	
#	ks = ks_2samp(aa,sorted)	
#	aou = abs(xa.sum() + 1 - lastx - .5)
	print "\mbox{%s} & %.3f & %.3f \\ " %   (mylegend[ik],ks,pks)

#	print "\mbox{%s} & %.3f & %.3f & " %   (mylegend[ik],ks[0],aou)
	pl.plot( sorted, ysorted ,colors[ik],marker=markers[ik],markevery=makeverys[ik],label=mylegend[ik] )
	pl.legend(loc=4)



pl.show()
