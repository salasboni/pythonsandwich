#! /usr/bin/env ipython

# Code source: Rebeca Salas-Boni
# Contact me at: salasboni@gmail.com

import numpy as np
import pylab as pl
from sklearn import neighbors, datasets, linear_model, svm
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
import csv 

studentfile = "/Users/newuser/astudentData.csv"
cr = csv.reader(open(studentfile,"rb"))

# skip header
cr.next() 
studnum = []
questnum = []
for row in cr:    
	questnum.append(row)
	studnum.append(row[0])

dquest = {}
dstud = {}
# loop that makes it work
for question, student, answer in questnum:
	dquest.setdefault(question, {})[student] = answer
	dstud.setdefault(student,{})[question] = answer
	

numstud = len(dstud.keys())
print "the number of students is %d" % numstud	

numques = len(dquest.keys())
print "the number of questions is %d" % numques	
numques = np.zeros(shape=(len(dquest.keys()))) # number of times each question is asked
numquesans = np.zeros(shape=(len(dquest.keys()))) # proportion of times each question is answered correctly

i=0
questno = []

for question in dquest.iteritems():
	newdic = dquest[question[0]]
	newdicv=newdic.values()		
	numques[i]=len(newdicv)
	if len(newdicv)<10:
		questno.append(question[0])	
	numquesans[i] =((np.array(newdicv).astype(np.int)).sum())*1./len(newdicv)
	#remove questions everyone answers correctly or incorrectly
	if numquesans[i]<.2 or numquesans[i]>.8:
		questno.append(question[0])
	i += 1
	
i = 0
studsno = []
dstudnew = {}
for question,student,answer in questnum:
	newdic = dstud[student]
	newdicv = newdic.values()	
	if len(newdicv)<5:
		studsno.append(student)
	i += 1

questnumnew=[]
for row in questnum:
	ques1=row[0]
	stud1 = [1]
	if ques1 not in questno and stud1 not in studsno:
		questnumnew.append(row)
		
dquest = {}
dstud = {}
# loop that makes it work
for question, student, answer in questnumnew:
	dquest.setdefault(question, {})[student] = answer
	dstud.setdefault(student,{})[question] = answer
	

numstud = len(dstud.keys())
print "the number of students is %d" % numstud	

numques = len(dquest.keys())
print "the number of questions is %d" % numques	

numques = np.zeros(shape=(len(dstud.keys()))) # number of question eaach student has
scorestu = np.zeros(shape=(len(dstud.keys()))) # score each student has
i = 0
for student in dstud.iteritems():
	newdic = dstud[student[0]]
	newdicv=newdic.values()		
	numques[i] = len(newdicv)	
	scorestu[i] =((np.array(newdicv).astype(np.int)).sum())*1./len(newdicv)
	i += 1

meanstu = np.mean(scorestu)
stdstu = np.std(scorestu)


fig = pl.figure()	
n, bins, patches = pl.hist(numques, 40, facecolor='red', alpha=0.75)

fig = pl.figure()	
n, bins, patches = pl.hist(scorestu, 30, facecolor='red', alpha=0.75)

numques = np.zeros(shape=(len(dquest.keys()))) # number of times each question is asked
numquesans = np.zeros(shape=(len(dquest.keys()))) # proportion of times each question is answered correctly

i=0
for question in dquest.iteritems():
	newdic = dquest[question[0]]
	newdicv=newdic.values()		
	numques[i]=len(newdicv)	
	numquesans[i] =((np.array(newdicv).astype(np.int)).sum())*1./len(newdicv)
	i += 1

fig = pl.figure()	
n, bins, patches = pl.hist(numques, 20, facecolor='green', alpha=0.75)
fig = pl.figure()	
n, bins, patches = pl.hist(numquesans, 20, facecolor='green', alpha=0.75)


i = 0
distances = np.zeros(shape=(len(dquest.keys()))) # distribution of scores of students who were asked this question, including
distances2 = np.zeros(shape=(len(dquest.keys()))) # distribution of scores of students who were asked this question, including

for question in dquest.iteritems():
	newdic = dquest[question[0]]	
	numstud = len(newdic.keys())
	numquesin = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, including
	numquesex = np.zeros(shape=(numstud)) # distribution of scores of students who were asked this question, excluding
	numques0 = [];numques1 = [];j = 0
	for student in newdic.keys():	
		newdicv = dstud[student]
		newdicv = newdicv.values()
		numquesin[j] =((np.array(newdicv).astype(np.int)).sum())*1./(len(newdicv))
		numquesex[j] =((np.array(newdicv).astype(np.int)).sum() - np.array(newdic[student]).astype(np.int))*1./(len(newdicv)-1)
		if newdic[student]=='0':
			numques0.append(numquesex[j])
		else:
			numques1.append(numquesex[j])
		j += 1
	
	meanin = np.mean(numquesin);meanex = np.mean(numquesex)
	stdin = np.std(numquesin);stdex = np.std(numquesin)
	distin = np.abs((meanin - meanstu)*1./(stdin + stdstu))
	distex = np.abs((meanex - meanstu)*1./(stdex + stdstu))
	distances2[i] = distex - distin
	mean0 = np.mean(numques0);mean1 = np.mean(numques1)
	std0 = np.std(numques0);std1 = np.std(numques1)
	dist = (mean1 - mean0)*1./(std0 + std1)
	distances[i]=dist
	newdic['distance'] = dist
	dquest[question[0]] = newdic
	i += 1
	
print "my distance"	
print distances	
print "levaaaants distance"
print distances2
distances = distances[np.logical_not(np.isnan(distances))]
distances2 = distances2[np.logical_not(np.isnan(distances2))]

distancesmin = np.sort(distances2)
distin = sorted(range(len(distances2)), key=lambda k: distances2[k])
distin = np.array(distin).astype(np.int)
distin = distin[0:19]
ques = dquest.keys()
ques = [ques[ij] for ij in distin]
print ques
fig = pl.figure()
print len(distances)
distancesmin = np.sort(distances)
distin = sorted(range(len(distances)), key=lambda k: distances[k])
distin = np.array(distin).astype(np.int)
distin = distin[0:19]
ques = dquest.keys()
ques = [ques[ij] for ij in distin]
print ques
distances = distances[distances>0]
print len(distances)
n, bins, patches = pl.hist(distances, 20, facecolor='blue', alpha=0.75)	
fig = pl.figure()	
n, bins, patches = pl.hist(distances2, 20, facecolor='red', alpha=0.75)	
pl.show()

